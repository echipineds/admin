<?php
session_start();
  require "DbConn.php";

$sqlpending = "SELECT intReserveCustomer_ID, strReserveStatus FROM reservationtbl WHERE reservationtbl.strReserveStatus = 'PENDING'";
$countpend = $conn->query($sqlpending);
$totalpending = mysqli_num_rows($countpend);

$sqlinbox = "SELECT * FROM messagetbl WHERE strMessage_ConvoCode = 'usersentunread'";
$countinbox = $conn->query($sqlinbox);  
$totalinbox = mysqli_num_rows($countinbox);
 ?>
 
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Priest Profile</title>
  <link rel="icon" type="image/ico" href="OLALOGOmin.png " />
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <a href="index2.php" class="logo">
      <span class="logo-mini"><img src="OLALOGO.png" style="position: relative; left: -18px;" /></span>
      <span class="logo-lg"><img src="OLALOGO.png" style="position: relative; left: -18px;" /></span>
    </a>

    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
    </nav>
  </header>

  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><center>MAIN NAVIGATION</center></li>
        <li class="treeview">
          <a href="index2.php">
            <i class="fa fa-dashboard"></i><span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-list"></i> <span>Transaction</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <small class="label pull-right bg-yellow"><?php echo $totalpending?></small>
              <small class="label pull-right bg-green"></small>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="PendReserve.php"><i class="fa fa-fw fa-circle"></i>Pending Requests</a></li>
            <li><a href="ConfirmReserve.php"><i class="fa fa-fw fa-circle"></i> Confirmed Requests</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-wrench"></i> <span>Maintenance</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="CalendarOfActivities.php"><i class="fa fa-calendar"></i> Calendar of Activities</a></li>
            <li><a href="Gospeloftheweek.php"><i class="fa fa-fw fa-file-text"></i> Gospel of the Week</a></li>
            <li><a href="Merchandise.php"><i class="fa fa-fw fa-shopping-cart"></i> Church Merchandise</a></li>
          </ul>
        </li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-fw fa-users"></i> <span>Profiles</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="PriestProfile.php"><i class="fa fa-fw fa-user"></i>Priest</a></li>
            <li><a href="CustomerProfile.php"><i class="fa fa-fw fa-user"></i>Parishioners</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-file-text"></i> <span>Documents</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="AddDoc.php"><i class="fa fa-fw fa-files-o"></i>Certificates</a></li>
            <li><a href="#"><i class="fa fa-fw fa-files-o"></i>Vouchers</a></li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-envelope"></i> <span>Mailbox</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <small class="label pull-right bg-blue"><?php echo $totalinbox?></small>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="MessageInbox.php"><i class="fa fa-fw fa-circle"></i>Inbox</a></li>
            <li><a href="MessageSent.php"><i class="fa fa-fw fa-circle"></i>Sent Items</a></li>
            <li><a href="MessageCompose.php"><i class="fa fa-fw fa-circle"></i>Compose message</a></li>
          </ul>
        </li>
        <li class="treeview-active">
          <a href="?s=logout">
            <i class="fa fa-sign-out"></i><span>Log out</span>
          </a>
        </li>
    </section>
  </aside>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Priest Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-fw fa-users"></i> Profiles</a></li>
        <li class="active">Priest Profile</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-8">
          <div class="box box-info" style="width:970px;">
            <div class="box-header with-border" style="height: 20px;">
            </div>

            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  
                  <?php 

                   $select_all_priest = "SELECT * FROM priesttbl";
              $select_query_result = $conn->query($select_all_priest);

                        if($select_query_result -> num_rows > 0) {
                          
                  echo "<tr>";
                    echo "<th><center>Photo</center></th>";
                    echo "<th><center>Priest Name</center></th>";
                    echo "<th><center>Position</center></th>";
                    echo "<th><center>Action</center></th>";
                  echo "</tr>";
                  echo "</thead>";
                  echo "<tbody>";
                  echo "<tr>";
                    
                          while($priesttbl = $select_query_result->fetch_assoc()) {
                      echo "<td>";                                                                            
                        echo "<center>";
                        echo '<img src="data:image/jpeg;base64,'.base64_encode($priesttbl['blobPriest_Photo']).' "class= img-circle width= "45px" alt="image">';  
                        echo "</center>";
                      echo "</td>";
                      echo "<td>"."<center>",$priesttbl['strPriest_FirstName']," ",$priesttbl['strPriest_LastName'],"</center>"."</td>";
                       echo "<td>"."<center>",$priesttbl['strPriest_Position'],"<center>"."</td>";            
                          echo "<td>";  
                           echo "<div style='display:block; float:right; position: relative; right: 125px;'>";
                           echo "<form action='ViewPriestSched.php' method='post'>";
                           echo "<button data-toggle='tooltip' name = 'id'  data-placement='top' title='View Schedule'  value='".$priesttbl['intPriestID']."' style='color: black; background-color: #337ab7; border-color: #2e6da4; border-radius: 50%;'>";
                            echo "<i class='fa fa-fw fa-eye'>";
                            echo "</i>";            
                          echo "</button>";
                          echo "</form>";
                           echo "</div>";
                             echo "<div style='display: block; float: right; position:relative; right:55px;'>";
                          echo "<form action='DeletePriest.php' method='post'>";
                          echo "<button type='submit' data-toggle='tooltip' data-placement='top' name='edit' title='Edit' value='".$priesttbl['intPriestID']."' style='color: black; background-color: #337ab7; border-color: #2e6da4; border-radius: 50%;'>";
                            echo "<i class='fa fa-fw fa-edit'>";
                            echo "</i>";
                          echo "</button>";
                         
                           echo "</div>";
                           echo "<div style='display: block; float: right; position:relative; right:-15px;'>";
                           
                          echo "<button type='submit' data-toggle='tooltip' value='".$priesttbl['intPriestID']."' data-placement='top' name='delete' title='Delete' style='color: black; background-color: #337ab7; border-color: #2e6da4; border-radius: 50%;'>";
                      
                            echo "<i class='fa fa-fw fa-trash-o'>";
                            echo "</i>";
                          echo "</button>";
                          echo "</form>";
                          echo "</div>";
                          echo "</td>";


                      echo "</tr>";
                            }
                           }
                           else {
                    echo"<h4><i class='fa fa-info'></i> Note:</h4>";
                    echo "<i>No Priest Available</i>";
                  }
                  ?>
                  </tbody>
                </table>
              </div>
              <div class="footer pull-right" style=" -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px;">
                  <a class="btn btn-primary" href="AddPriest.php" role="button">+ Add Priest</a>
                </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  
  <div class="control-sidebar-bg"></div>

</div>

<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!--<script>
  function SomeDeleteRowFunction(btndel) {
    if (typeof(btndel) == "object") {
        $(btndel).closest("tr").remove();
    } else {
        return false;
    }
}</script>-->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<script src="dist/js/adminlte.min.js"></script>
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="bower_components/chart.js/Chart.js"></script>
<script src="dist/js/pages/dashboard2.js"></script>
<script src="dist/js/demo.js"></script>
</body>
</html>
