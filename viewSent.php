<?php
session_start();
include "DbConn.php";
$sqlpending = "SELECT intReserveCustomer_ID, strReserveStatus FROM reservationtbl WHERE reservationtbl.strReserveStatus = 'PENDING'";
$countpend = $conn->query($sqlpending);
$totalpending = mysqli_num_rows($countpend);

$sqlinbox = "SELECT * FROM messagetbl WHERE strMessage_ConvoCode = 'usersentunread'";
$countinbox = $conn->query($sqlinbox);  
$totalinbox = mysqli_num_rows($countinbox);
?>
<?php

  if(isset($_POST['read'])) {
    $messageid = $_POST['read'];
    
    $view_message= "UPDATE `messagetbl` SET `strMessage_ConvoCode` = 'adminsentread' WHERE `messagetbl`.`intMessageID` = '$messageid'";
    $view_message_query = $conn->query($view_message);  
  }
 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Read message</title>
  <link rel="icon" type="image/ico" href="OLALOGOmin.png " />
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css"> 
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <a href="index2.php" class="logo">
      <span class="logo-mini"><img src="OLALOGO.png" style="position: relative; left: -18px;" /></span>
      <span class="logo-lg"><img src="OLALOGO.png" style="position: relative; left: -18px;" /></span>
    </a>

    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
    </nav>
  </header>

  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><center>MAIN NAVIGATION</center></li>
        <li class="treeview">
          <a href="index2.php">
            <i class="fa fa-dashboard"></i><span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-list"></i> <span>Transaction</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <small class="label pull-right bg-yellow"><?php echo $totalpending?></small>
              <small class="label pull-right bg-green"></small>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="PendReserve.php"><i class="fa fa-fw fa-circle"></i> Pending Requests</a></li>
            <li><a href="ConfirmReserve.php"><i class="fa fa-fw fa-circle"></i> Confirmed Requests</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-wrench"></i> <span>Maintenance</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="CalendarOfActivities.php"><i class="fa fa-calendar"></i> Calendar of Activities</a></li>
            <li><a href="Gospeloftheweek.php"><i class="fa fa-fw fa-file-text"></i> Gospel of the Week</a></li>
            <li><a href="Merchandise.php"><i class="fa fa-fw fa-shopping-cart"></i> Church Merchandise</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-users"></i> <span>Profiles</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="PriestProfile.php"><i class="fa fa-fw fa-user"></i>Priest</a></li>
            <li><a href="CustomerProfile.php"><i class="fa fa-fw fa-user"></i>Parisioners</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-file-text"></i> <span>Documents</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="AddDoc.php"><i class="fa fa-fw fa-files-o"></i>Certificates</a></li>
            <li><a href="#"><i class="fa fa-fw fa-files-o"></i>Vouchers</a></li>
          </ul>
        </li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-fw fa-envelope"></i> <span>Mailbox</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <small class="label pull-right bg-blue"><?php echo $totalinbox?></small>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="MessageInbox.php"><i class="fa fa-fw fa-circle"></i>Inbox</a></li>
            <li><a href="MessageSent.php"><i class="fa fa-fw fa-check-circle"></i>Sent Items</a></li>
            <li><a href="MessageCompose.php"><i class="fa fa-fw fa-circle"></i>Compose message</a></li>
          </ul>
        </li>
        <li class="treeview-active">
          <a href="?s=logout">
            <i class="fa fa-sign-out"></i><span>Log out</span>
          </a>
        </li>
    </section>
  </aside>

    <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Sent Item
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-fw  fa-envelope-o"></i>Mailbox</a></li>
        <li><a href="MessageSent.php"><i class="fa fa-fw  fa-envelope-o"></i>Sent Item</a></li>
        <li class="active">Read message</li>
      </ol>
    </section>

    <?php
    echo "<section class='content'>";
      echo "<div class='row'>";
        echo "<div class='col-md-9'>";
          echo "<div class='box box-primary'>";
            echo "<div class='box-header with-border'>";
              echo "<h3 class='box-title'>Read Mail</h3>";
           echo "</div>";
            $select_all_message = "SELECT messagetbl.intMessageID, messagetbl.strMessage_ConvoCode, messagetbl.intMessage_UserID,messagetbl.strMessage_Receiver, messagetbl.strMessage_Content, usertbl.intUserID, usertbl.strUsername FROM messagetbl INNER JOIN usertbl ON messagetbl.intMessage_UserID = usertbl.intUserID WHERE intMessageID = $messageid";
            $select_query_message = $conn->query($select_all_message);
            if($select_query_message -> num_rows > 0) {    
              while($messagesent = $select_query_message->fetch_assoc()) {
            echo "<div class='box-body no-padding'>";
              echo "<div class='mailbox-read-info'>";
                echo "<h5>From:",$messagesent['strMessage_Receiver'],"<span class='mailbox-read-time pull-right'></span></h5>";
              echo "</div>";
              echo "<div class='mailbox-read-message'>";
                echo "<p>",$messagesent['strMessage_Content'],"</p>";
              echo "</div>";
               echo "</div>";

            echo "<div class='box-footer'>";
              
               echo "<div style='display:block; float:right; position: relative; right: 110px;'>";
                echo "<button type='button' class='btn btn-default'><i class='fa fa-reply'><a href='MessageSent.php'></i>Back</button>";
                echo "</div>";
                echo "<div style='display:block; float:right; position: relative; right: -40px;'>";
                echo "<form action='Deletesentmessage.php' method='post'>";
                echo "<button type='submit' name = 'delete' value = '".$messagesent['intMessageID']."' class='btn btn-default'><i class='fa fa-trash'></i>Delete</button>";
                echo "</form>";
              echo "</div>";
            echo "</div>";
          }
        }
          echo "</div>";
        echo "</div>";
      echo "</div>";
    echo "</section>";
    ?>
 
  </div>
</section>

  <div class="control-sidebar-bg"></div>

</div>

<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<script src="dist/js/adminlte.min.js"></script>
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="bower_components/chart.js/Chart.js"></script>
<script src="dist/js/pages/dashboard2.js"></script>
<script src="dist/js/demo.js"></script>
</body>
</html>
