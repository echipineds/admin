<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Reports</title>
  <link rel="icon" type="image/ico" href="OLALOGOmin.png " />
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body onload="window.print();">
  <div class="wrapper">
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <span class="logo-mini"><img src="OLALOGO.png" style="position: relative; left: -18px;" /></span>
            <small class="pull-right">Date: 2/10/2014</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
           <p class="lead"><strong>Transaction Report</strong></p>
          <i>October 1, 2018&nbsp;-&nbsp;October 31, 2018</i>
        </div>
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Name</th>
              <th>Contact </th>
              <th>Service</th>
              <th>Date</th>
              <th>Time</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>Charlotte Dolores</td>
              <td>09182668034</td>
              <td>Matrimony</td>
              <td>October 18, 2018</td>
              <td>12:00PM</td>
            </tr>
            <tr>
              <td>Aira May De Rosas</td>
              <td>09182795034</td>
              <td>Baptism</td>
              <td>November 11, 2018</td>
              <td>2:00PM</td>
            </tr>
            <tr>
              <td>Ranet San Felipe</td>
              <td>09136868034</td>
              <td>Baptism</td>
              <td>October 11, 2018</td>
              <td>8:00PM</td>
            </tr>
            <tr>
              <td>Hamlet Dolores</td>
              <td>09182668751</td>
              <td>Funeral Mass</td>
              <td>October 12, 2018</td>
              <td>12:00PM</td>
            </tr>
            <tr>
              <td>Charlotte Dolores</td>
              <td>09182668034</td>
              <td>Matrimony</td>
              <td>October 18, 2018</td>
              <td>12:00PM</td>
            </tr>
            <tr>
              <td>Aira May De Rosas</td>
              <td>09182795034</td>
              <td>Baptism</td>
              <td>November 11, 2018</td>
              <td>2:00PM</td>
            </tr>
            <tr>
              <td>Ranet San Felipe</td>
              <td>09136868034</td>
              <td>Baptism</td>
              <td>October 11, 2018</td>
              <td>8:00PM</td>
            </tr>
            <tr>
              <td>Hamlet Dolores</td>
              <td>09182668751</td>
              <td>Funeral Mass</td>
              <td>October 12, 2018</td>
              <td>12:00PM</td>
            </tr>
            <tr>
              <td>Charlotte Dolores</td>
              <td>09182668034</td>
              <td>Matrimony</td>
              <td>October 18, 2018</td>
              <td>12:00PM</td>
            </tr>
            <tr>
              <td>Aira May De Rosas</td>
              <td>09182795034</td>
              <td>Baptism</td>
              <td>November 11, 2018</td>
              <td>2:00PM</td>
            </tr>
            <tr>
              <td>Ranet San Felipe</td>
              <td>09136868034</td>
              <td>Baptism</td>
              <td>October 11, 2018</td>
              <td>8:00PM</td>
            </tr>
            <tr>
              <td>Hamlet Dolores</td>
              <td>09182668751</td>
              <td>Funeral Mass</td>
              <td>October 12, 2018</td>
              <td>12:00PM</td>
            </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        
        <div class="col-xs-6">
          <p class="lead">Overall Number of Reservations by Services</p>

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Baptism:</th>
                <td>2</td>
              </tr>
              <tr>
                <th>Confirmation</th>
                <td>2</td>
              </tr>
              <tr>
                <th>Matrimony:</th>
                <td>1</td>
              </tr>
              <tr>
                <th>Funeral Mass:</th>
                <td>0</td>
              </tr>
              <tr>
                <th>Mass Intention:</th>
                <td>0</td>
              </tr>
              <tr>
                <th>Blessing:</th>
                <td>3</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </section>

</div>
</body>
</html>
