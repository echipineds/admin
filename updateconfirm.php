<?php

 require "DbConn.php";
    session_start();
   if(isset($_POST['unconfirm'])) {
  

    $id = $_POST['unconfirm'];
    $bapid = $_POST['intBaptism_ID'];
    $confid = $_POST['intConf_ID'];
    $wedid = $_POST['intWedding_ID'];
    $miid = $_POST['intMInt_ID'];
    $fmid = $_POST['intFuneralMass_ID'];
    $bsid = $_POST['intBlessingServ_ID']; 
    $cerid = $_POST['intCertificateID'];
    $service = $_POST['strTypeofService'];
    $status = 'PENDING';
    

    if ($service = 'BAPTISM'){
    $update_confirm = "UPDATE reservationtbl, servicetbl SET strReserveStatus = '$status' WHERE intReserveCustomer_ID = '$id' AND intBaptism_ID = '$bapid' AND strTypeofService = '$service'";
    $update_confirm_query = $conn->query($update_confirm);
     }
    if ($service = 'CONFIRMATION'){
      $update_confirm = "UPDATE reservationtbl, servicetbl SET strReserveStatus = '$status' WHERE intReserveCustomer_ID = '$id' AND intConf_ID = '$confid' AND strTypeofService = '$service'";
      $update_confirm_query = $conn->query($update_confirm);
     }
    if ($service = 'WEDDING'){
      $update_confirm = "UPDATE reservationtbl, servicetbl SET strReserveStatus = '$status' WHERE intReserveCustomer_ID = '$id' AND intWedding_ID = '$wedid' AND strTypeofService = '$service'";
      $update_confirm_query = $conn->query($update_confirm);
     }
     if ($service = 'MASS INTENTION'){
      $update_confirm = "UPDATE reservationtbl, servicetbl SET strReserveStatus = '$status' WHERE intReserveCustomer_ID = '$id' AND intMInt_ID = '$miid' AND strTypeofService = '$service'";
      $update_confirm_query = $conn->query($update_confirm);
     }
     if ($service = 'FUNERAL MASS'){
      $update_confirm = "UPDATE reservationtbl, servicetbl SET strReserveStatus = '$status' WHERE intReserveCustomer_ID = '$id' AND intFuneralMass_ID = '$fmid' AND strTypeofService = '$service'";
      $update_confirm_query = $conn->query($update_confirm);
     }
     if ($service = 'BLESSING'){
      $update_confirm = "UPDATE reservationtbl, servicetbl SET strReserveStatus = '$status' WHERE intReserveCustomer_ID = '$id' AND intBlessingServ_ID = '$bsid' AND strTypeofService = '$service'";
      $update_confirm_query = $conn->query($update_confirm);
     }
     if ($service = 'CERTIFICATION'){
      $update_confirm = "UPDATE reservationtbl, servicetbl SET strReserveStatus = '$status' WHERE intReserveCustomer_ID = '$id' AND intCertificateID = '$cerid' AND strTypeofService = '$service'";
      $update_confirm_query = $conn->query($update_confirm);
     }
     
  if($update_confirm_query){

    $Editunconfirm_Msg = "<div class='alert alert-info text-center'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    Successfully unconfirmed reservation!
    </div>";
    header("refresh:0.5 url=ConfirmReserve.php");
  }
  else{
    $Editunconfirm_Msg = "<div class='alert alert-info text-center'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    Failed to unconfirm reservation!
    </div>";
    header("refresh:0.5 url=ConfirmReserve.php");
  }

}

if(isset($_POST['delete'])) {
    
  $del_id = $_POST['delete'];
    $bapid = $_POST['intBaptism_ID'];
    $confid = $_POST['intConf_ID'];
    $wedid = $_POST['intWedding_ID'];
    $miid = $_POST['intMInt_ID'];
    $fmid = $_POST['intFuneralMass_ID'];
    $bsid = $_POST['intBlessingServ_ID']; 
    $cerid = $_POST['intCertificateID'];
    $resid = $_POST['intReservationID'];
    $service = $_POST['strTypeofService'];

   if ($service = 'BAPTISM'){
   $del_conf = "DELETE FROM reservationtbl WHERE intReserveCustomer_ID = '$del_id' AND intReservationID = '$resid' AND strTypeofService = '$service'"; 
   $del_query = $conn->query($del_conf);
   if(del_query) {
     
      $del_conf2 = "DELETE FROM servicetbl WHERE intBaptism_ID = '$bapid'"; 
      $del_query2 = $conn->query($del_conf2);
      
      if(del_query2) {
        $del_conf3 = "DELETE FROM baptismservtbl WHERE intBapID = '$bapid'";
        $del_query3 = $conn->query($del_conf3);
        if(del_query3){
      echo "<script>alert('Successfully deleted!');</script>";
      header('Location: ConfirmReserve.php');
          }
      }
    }
  }
  if ($service = 'CONFIRMATION'){
   $del_conf = "DELETE FROM reservationtbl WHERE intReserveCustomer_ID = '$del_id' AND intReservationID = '$resid' AND strTypeofService = '$service'"; 
   $del_conf = $conn->query($del_conf);
   if(del_query) {
     
      $del_conf2 = "DELETE FROM servicetbl WHERE intConf_ID = '$confid'"; 
      $del_query2 = $conn->query($del_conf2);
      
      if(del_query2) {
        $del_conf3 = "DELETE FROM confirmationservtbl WHERE intConfID= '$confid'";
        $del_query3 = $conn->query($del_conf3);
        if(del_query3){
      echo "<script>alert('Successfully deleted!');</script>";
      header('Location: PendReserve.php');
          }
      }
    }
  }
  if ($service = 'WEDDING'){
   $del_conf = "DELETE FROM reservationtbl, servicetbl WHERE intReserveCustomer_ID = '$del_id' AND intReservationID = '$resid' AND strTypeofService = '$service'"; 
   $del_query = $conn->query($del_conf);
   if(del_query) {
     
      $del_conf2 = "DELETE FROM servicetbl WHERE intWedding_ID = '$wedid'"; 
      $del_query2 = $conn->query($del_conf2);
      
      if(del_query2) {
        $del_conf3 = "DELETE FROM weddingservtbl WHERE intWeddingID= '$wedid'";
        $del_query3 = $conn->query($del_conf3);
        if(del_query3){
      echo "<script>alert('Successfully deleted!');</script>";
      header('Location: ConfirmReserve.php');
          }
      }
    }
  }
  if ($service = 'MASS INTENTION'){
   $del_conf = "DELETE FROM reservationtbl WHERE intReserveCustomer_ID = '$del_id' AND intReservationID = '$resid' AND strTypeofService = '$service'"; 
   $del_query = $conn->query($del_conf);
   if(del_query) {
     
      $del_conf2 = "DELETE FROM servicetbl WHERE intMInt_ID = '$miid'"; 
      $del_query2 = $conn->query($del_conf2);
      
      if(del_query2) {
        $del_conf3 = "DELETE FROM massintservid WHERE intMIntID = '$miid'";
        $del_query3 = $conn->query($del_conf3);
        if(del_query3){
      echo "<script>alert('Successfully deleted!');</script>";
      header('Location: ConfirmReserve.php');
          }
      }
    }
  }
  if ($service = 'FUNERAL MASS'){
   $del_conf = "DELETE FROM reservationtbl WHERE intReserveCustomer_ID = '$del_id' AND intReservationID = '$resid' AND strTypeofService = '$service'"; 
   $del_query = $conn->query($del_conf);
   if(del_query) {
     
      $del_conf2 = "DELETE FROM servicetbl WHERE intFuneralMass_ID = '$fmid'"; 
      $del_query2 = $conn->query($del_conf2);
      
      if(del_query2) {
        $del_conf3 = "DELETE FROM funeralmassservtbl WHERE intFuneralID = '$fmid'";
        $del_query3 = $conn->query($del_conf3);
        if(del_query3){
      echo "<script>alert('Successfully deleted!');</script>";
      header('Location: ConfirmReserve.php');
          }
      }
    }
  }
  if ($service = 'BLESSING'){
   $del_conf = "DELETE FROM reservationtbl WHERE intReserveCustomer_ID = '$del_id' AND intReservationID = '$resid' AND strTypeofService = '$service'"; 
   $del_query = $conn->query($del_conf);
   if(del_query) {
     
      $del_pend2 = "DELETE FROM servicetbl WHERE intBlessingServ_ID = '$bsid'"; 
      $del_query2 = $conn->query($del_conf2);
      
      if(del_query2) {
        $del_conf3 = "DELETE FROM blessingservtbl WHERE intBlessingID = '$bsid'";
        $del_query3 = $conn->query($del_conf3);
        if(del_query3){
      echo "<script>alert('Successfully deleted!');</script>";
      header('Location: ConfirmReserve.php');
          }
      }
    }
  }
  if ($service = 'CERTIFICATION'){
   $del_conf = "DELETE FROM reservationtbl WHERE intReserveCustomer_ID = '$del_id' AND intReservationID = '$resid' AND strTypeofService = '$service'"; 
   $del_query = $conn->query($del_conf);
   if(del_query) {
     
      $del_conf2 = "DELETE FROM servicetbl WHERE intCertificateID = '$cerid'"; 
      $del_query2 = $conn->query($del_conf2);
      
      if(del_query2) {
        $del_conf3 = "DELETE FROM certificatetbl WHERE intCertID = '$cerid'";
        $del_query3 = $conn->query($del_conf3);
        if(del_query3){
      echo "<script>alert('Successfully deleted!');</script>";
      header('Location: ConfirmReserve.php');
          }
      }
    }
  }
}
    
  else {
    // if there are no post variable are preset
    header('Location: ConfirmReserve.php');
    
  }
