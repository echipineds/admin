<?php

 require "DbConn.php";
    session_start();
   if(isset($_POST['confirm'])) {

    $id = $_POST['confirm'];
    $bapid = $_POST['intBaptism_ID'];
    $confid = $_POST['intConf_ID'];
    $wedid = $_POST['intWedding_ID'];
    $miid = $_POST['intMInt_ID'];
    $fmid = $_POST['intFuneralMass_ID'];
    $bsid = $_POST['intBlessingServ_ID']; 
    $cerid = $_POST['intCertificateID'];
    $service = $_POST['strTypeofService'];
    $status = 'CONFIRMED';
    $custname = $_POST['strCustomer_Name'];
    $resdate = $_POST['dtReserve_Date'];
    $restime = $_POST['tmReserve_Time'];
    $resendtime = $_POST['tmReserve_End_Time'];
    $massage = 'Your reservation has been confirmed';
    $massagedel = 'Your reservation has been deleted';
    $mcode = 'adminconf';
    $adminid = '1';
    $now = date('Y-m-d H:i:s',time());
    $color = '0071C5';
    $res_concat_start = date('Y-m-d H:i:s', strtotime("$resdate $restime"));
    $res_concat_end = date('Y-m-d H:i:s', strtotime("$resdate $resendtime"));

    if ($service != 'MASS INTENTION' && $service != 'CERTIFICATION' ){
    $sql = "INSERT INTO eventstbl(title, start, end, color) values ('$service', '$res_concat_start', '$res_concat_end', '$color')";
    $sql_query = $conn->query($sql);
  }
    
    if ($service = 'BAPTISM'){
    $update_pending = "UPDATE reservationtbl, servicetbl SET strReserveStatus = '$status' WHERE intReserveCustomer_ID = '$id' AND intBaptism_ID = '$bapid' AND strTypeofService = '$service'";
    $update_pending_query = $conn->query($update_pending);
     }
    if ($service = 'CONFIRMATION'){
      $update_pending = "UPDATE reservationtbl, servicetbl SET strReserveStatus = '$status' WHERE intReserveCustomer_ID = '$id' AND intConf_ID = '$confid' AND strTypeofService = '$service'";
      $update_pending_query = $conn->query($update_pending2);
     }
    if ($service = 'WEDDING'){
      $update_pending = "UPDATE reservationtbl, servicetbl SET strReserveStatus = '$status' WHERE intReserveCustomer_ID = '$id' AND intWedding_ID = '$wedid' AND strTypeofService = '$service'";
      $update_pending_query = $conn->query($update_pending);
     }
     if ($service = 'MASS INTENTION'){
      $update_pending = "UPDATE reservationtbl, servicetbl SET strReserveStatus = '$status' WHERE intReserveCustomer_ID = '$id' AND intMInt_ID = '$miid' AND strTypeofService = '$service'";
      $update_pending_query = $conn->query($update_pending);
     }
     if ($service = 'FUNERAL MASS'){
      $update_pending = "UPDATE reservationtbl, servicetbl SET strReserveStatus = '$status' WHERE intReserveCustomer_ID = '$id' AND intFuneralMass_ID = '$fmid' AND strTypeofService = '$service'";
      $update_pending_query = $conn->query($update_pending);
     }
     if ($service = 'BLESSING'){
      $update_pending = "UPDATE reservationtbl, servicetbl SET strReserveStatus = '$status' WHERE intReserveCustomer_ID = '$id' AND intBlessingServ_ID = '$bsid' AND strTypeofService = '$service'";
      $update_pending_query = $conn->query($update_pending);
     }
     if ($service = 'CERTIFICATION'){
      $update_pending = "UPDATE reservationtbl, servicetbl SET strReserveStatus = '$status' WHERE intReserveCustomer_ID = '$id' AND intCertificateID = '$cerid' AND strTypeofService = '$service'";
      $update_pending_query = $conn->query($update_pending);
     }

  if($update_pending_query){
     $sendconfirm = "INSERT INTO messagetbl (strMessage_ConvoCode, strMessage_Content, intMessage_UserID, strMessage_Receiver, dtmMessage_Sent) VALUES ('$mcode', '$massage', '$adminid', '$custname', '$now')";
    $send_confirm_query = $conn->query($sendconfirm);

    header("refresh:0.5 url=PendReserve.php");
  }
  else{
    $Editpending_Msg = "<div class='alert alert-info text-center'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    Confirming reservation failed!
    </div>";
    header("refresh:0.5 url=PendReserve.php");
  }

}

if(isset($_POST['delete'])) {
    
    $del_id = $_POST['delete'];
    $bapid = $_POST['intBaptism_ID'];
    $confid = $_POST['intConf_ID'];
    $wedid = $_POST['intWedding_ID'];
    $miid = $_POST['intMInt_ID'];
    $fmid = $_POST['intFuneralMass_ID'];
    $bsid = $_POST['intBlessingServ_ID']; 
    $cerid = $_POST['intCertificateID'];
    $resid = $_POST['intReservationID'];
    $service = $_POST['strTypeofService'];
    $massagedel = 'Your reservation has been cancelled';

  $senddel = "INSERT INTO messagetbl (strMessage_ConvoCode, strMessage_Content, intMessage_UserID, strMessage_Receiver, dtmMessage_Sent) VALUES ('$mcode', '$massagedel', '$adminid', '$custname', '$now')";
  $send_del_query = $conn->query($senddel);   
   
  if ($service = 'BAPTISM'){
   $del_pend = "DELETE FROM reservationtbl WHERE intReserveCustomer_ID = '$del_id' AND intReservationID = '$resid' AND strTypeofService = '$service'"; 
   $del_query = $conn->query($del_pend);
   if(del_query) {
     
      $del_pend2 = "DELETE FROM servicetbl WHERE intBaptism_ID = '$bapid'"; 
      $del_query2 = $conn->query($del_pend2);
      
      if(del_query2) {
        $del_pend3 = "DELETE FROM baptismservtbl WHERE intBapID = '$bapid'";
        $del_query3 = $conn->query($del_pend3);
        if(del_query3){
          
      echo "<script>alert('Successfully deleted!');</script>";
      header('Location: PendReserve.php');
          }
      }
    }
  }
  if ($service = 'CONFIRMATION'){
   $del_pend = "DELETE FROM reservationtbl WHERE intReserveCustomer_ID = '$del_id' AND intReservationID = '$resid' AND strTypeofService = '$service'"; 
   $del_query = $conn->query($del_pend);
   if(del_query) {
     
      $del_pend2 = "DELETE FROM servicetbl WHERE intConf_ID = '$confid'"; 
      $del_query2 = $conn->query($del_pend2);
      
      if(del_query2) {
        $del_pend3 = "DELETE FROM confirmationservtbl WHERE intConfID= '$confid'";
        $del_query3 = $conn->query($del_pend3);
        if(del_query3){
      echo "<script>alert('Successfully deleted!');</script>";
      header('Location: PendReserve.php');
          }
      }
    }
  }
  if ($service = 'WEDDING'){
   $del_pend = "DELETE FROM reservationtbl, servicetbl WHERE intReserveCustomer_ID = '$del_id' AND intReservationID = '$resid' AND strTypeofService = '$service'"; 
   $del_query = $conn->query($del_pend);
   if(del_query) {
     
      $del_pend2 = "DELETE FROM servicetbl WHERE intWedding_ID = '$wedid'"; 
      $del_query2 = $conn->query($del_pend2);
      
      if(del_query2) {
        $del_pend3 = "DELETE FROM weddingservtbl WHERE intWeddingID= '$wedid'";
        $del_query3 = $conn->query($del_pend3);
        if(del_query3){
      echo "<script>alert('Successfully deleted!');</script>";
      header('Location: PendReserve.php');
          }
      }
    }
  }
  if ($service = 'MASS INTENTION'){
   $del_pend = "DELETE FROM reservationtbl WHERE intReserveCustomer_ID = '$del_id' AND intReservationID = '$resid' AND strTypeofService = '$service'"; 
   $del_query = $conn->query($del_pend);
   if(del_query) {
     
      $del_pend2 = "DELETE FROM servicetbl WHERE intMInt_ID = '$miid'"; 
      $del_query2 = $conn->query($del_pend2);
      
      if(del_query2) {
        $del_pend3 = "DELETE FROM massintservid WHERE intMIntID = '$miid'";
        $del_query3 = $conn->query($del_pend3);
        if(del_query3){
      echo "<script>alert('Successfully deleted!');</script>";
      header('Location: PendReserve.php');
          }
      }
    }
  }
  if ($service = 'FUNERAL MASS'){
   $del_pend = "DELETE FROM reservationtbl WHERE intReserveCustomer_ID = '$del_id' AND intReservationID = '$resid' AND strTypeofService = '$service'"; 
   $del_query = $conn->query($del_pend);
   if(del_query) {
     
      $del_pend2 = "DELETE FROM servicetbl WHERE intFuneralMass_ID = '$fmid'"; 
      $del_query2 = $conn->query($del_pend2);
      
      if(del_query2) {
        $del_pend3 = "DELETE FROM funeralmassservtbl WHERE intFuneralID = '$fmid'";
        $del_query3 = $conn->query($del_pend3);
        if(del_query3){
      echo "<script>alert('Successfully deleted!');</script>";
      header('Location: PendReserve.php');
          }
      }
    }
  }
  if ($service = 'BLESSING'){
   $del_pend = "DELETE FROM reservationtbl WHERE intReserveCustomer_ID = '$del_id' AND intReservationID = '$resid' AND strTypeofService = '$service'"; 
   $del_query = $conn->query($del_pend);
   if(del_query) {
     
      $del_pend2 = "DELETE FROM servicetbl WHERE intBlessingServ_ID = '$bsid'"; 
      $del_query2 = $conn->query($del_pend2);
      
      if(del_query2) {
        $del_pend3 = "DELETE FROM blessingservtbl WHERE intBlessingID = '$bsid'";
        $del_query3 = $conn->query($del_pend3);
        if(del_query3){
      echo "<script>alert('Successfully deleted!');</script>";
      header('Location: PendReserve.php');
          }
      }
    }
  }
  if ($service = 'CERTIFICATION'){
   $del_pend = "DELETE FROM reservationtbl WHERE intReserveCustomer_ID = '$del_id' AND intReservationID = '$resid' AND strTypeofService = '$service'"; 
   $del_query = $conn->query($del_pend);
   if(del_query) {
     
      $del_pend2 = "DELETE FROM servicetbl WHERE intCertificateID = '$cerid'"; 
      $del_query2 = $conn->query($del_pend2);
      
      if(del_query2) {
        $del_pend3 = "DELETE FROM certificatetbl WHERE intCertID = '$cerid'";
        $del_query3 = $conn->query($del_pend3);
        if(del_query3){
      echo "<script>alert('Successfully deleted!');</script>";
      header('Location: PendReserve.php');
          }
      }
    }
  }

}    
  else {
    // if there are no post variable are preset
    header('Location: PendReserve.php');
    
  }
