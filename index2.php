<?php
session_start();

include "DbConn.php";

$sqlpending = "SELECT intReserveCustomer_ID, strReserveStatus FROM reservationtbl WHERE reservationtbl.strReserveStatus = 'PENDING'";
$countpend = $conn->query($sqlpending);
$totalpending = mysqli_num_rows($countpend);

$sqlinbox = "SELECT * FROM messagetbl WHERE strMessage_ConvoCode = 'usersentunread'";
$countinbox = $conn->query($sqlinbox);  
$totalinbox = mysqli_num_rows($countinbox);

?>

<?php
if(isset($_SESSION['intUserID']) && !empty($_SESSION['intUserID'])) {
    if($_SESSION['intUserID'] != '1') {
      header ("Location: Signin.php");
    } 
    else {
    }
  }
  else {
    header ("Location: Signin.php");
  }
  ?>
<?php
      if(isset($_GET['s']) && $_GET['s'] == 'logout') {
      session_destroy();      
      if($conn) {
        $conn->close();
      }
      header("Location: " . $_SERVER['PHP_SELF']);      
      }
?>

<?php
  $firsthalf = "January-June";
  $secondhalf = "July-December";
  $curdate = date("Y-m-d");
  $currenthalf = date('m',strtotime($curdate));

?>


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin Home</title>
  <link rel="icon" type="image/ico" href="OLALOGOmin.png " />
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <a href="index2.php" class="logo">
      <span class="logo-mini"><img src="OLALOGO.png" style="position: relative; left: -18px;" /></span>
      <span class="logo-lg"><img src="OLALOGO.png" style="position: relative; left: -18px;" /></span>
    </a>

    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>   
    </nav>
  </header>

  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><center>MAIN NAVIGATION</center></li>
        <li class="active treeview">
          <a href="index2.php">
            <i class="fa fa-dashboard"></i><span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-list"></i> <span>Transaction</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <small class="label pull-right bg-yellow"><?php echo $totalpending?></small>
              <small class="label pull-right bg-green"></small>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="PendReserve.php"><i class="fa fa-fw fa-circle"></i>Pending Requests</a></li>
            <li><a href="ConfirmReserve.php"><i class="fa fa-fw fa-circle"></i> Confirmed Requests</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-wrench"></i> <span>Maintenance</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="CalendarOfActivities.php"><i class="fa fa-calendar"></i> Calendar of Activities</a></li>
            <li><a href="Gospeloftheweek.php"><i class="fa fa-fw fa-file-text"></i> Gospel of the Week</a></li>
            <li><a href="Merchandise.php"><i class="fa fa-fw fa-shopping-cart"></i> Church Merchandise</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-users"></i> <span>Profiles</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="PriestProfile.php"><i class="fa fa-fw fa-user"></i>Priest</a></li>
            <li><a href="CustomerProfile.php"><i class="fa fa-fw fa-user"></i>Parishioners</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-file-text"></i> <span>Documents</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="AddDoc.php"><i class="fa fa-fw fa-files-o"></i>Certificates</a></li>
            <li><a href="#"><i class="fa fa-fw fa-files-o"></i>Vouchers</a></li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-envelope"></i> <span>Mailbox</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <small class="label pull-right bg-blue"><?php echo $totalinbox?></small>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="MessageInbox.php"><i class="fa fa-fw fa-circle"></i>Inbox</a></li>
            <li><a href="MessageSent.php"><i class="fa fa-fw fa-circle"></i>Sent Items</a></li>
            <li><a href="MessageCompose.php"><i class="fa fa-fw fa-circle"></i>Compose message</a></li>
          </ul>
        </li>
        <li class="treeview-active">
          <a href="?s=logout">
            <i class="fa fa-sign-out"></i><span>Log out</span>
          </a>
        </li>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <div class="form-group">
                <h3 class="box-title">Confirmed Reservations</h3>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <?php 
                  $sqlbapjan = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BAPTISM' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 1";
                  $sqlconjan = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'CONFIRMATION' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 1";
                  $sqlmatjan = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'WEDDING' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 1";
                  $sqlfunjan = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'FUNERAL MASS' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 1";
                  $sqlmasjan = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'MASS INTENTION' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 1";
                  $sqlblesjan = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BLESSING' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 1";
                  $connbapjan = $conn->query($sqlbapjan);
                  $connconjan = $conn->query($sqlconjan);
                  $connmatjan = $conn->query($sqlmatjan);
                  $connfunjan = $conn->query($sqlfunjan);
                  $connmasjan = $conn->query($sqlmasjan);
                  $connblesjan = $conn->query($sqlblesjan);
                  $countbapjan = mysqli_num_rows($connbapjan);
                  $countconjan = mysqli_num_rows($connconjan);
                  $countmatjan = mysqli_num_rows($connmatjan);
                  $countfunjan = mysqli_num_rows($connfunjan);
                  $countmasjan = mysqli_num_rows($connmasjan);
                  $countblesjan = mysqli_num_rows($connblesjan);

                  $sqlbapfeb = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BAPTISM' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 2";
                  $sqlconfeb = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'CONFIRMATION' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 2";
                  $sqlmatfeb = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'WEDDING' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 2";
                  $sqlfunfeb = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'FUNERAL MASS' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 2";
                  $sqlmasfeb = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'MASS INTENTION' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 2";
                  $sqlblesfeb = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BLESSING' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 2";
                  $connbapfeb = $conn->query($sqlbapfeb);
                  $connconfeb = $conn->query($sqlconfeb);
                  $connmatfeb = $conn->query($sqlmatfeb);
                  $connfunfeb = $conn->query($sqlfunfeb);
                  $connmasfeb = $conn->query($sqlmasfeb);
                  $connblesfeb = $conn->query($sqlblesfeb);
                  $countbapfeb = mysqli_num_rows($connbapfeb);
                  $countconfeb = mysqli_num_rows($connconfeb);
                  $countmatfeb = mysqli_num_rows($connmatfeb);
                  $countfunfeb = mysqli_num_rows($connfunfeb);
                  $countmasfeb = mysqli_num_rows($connmasfeb);
                  $countblesfeb = mysqli_num_rows($connblesfeb);

                  $sqlbapmar = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BAPTISM' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 3";
                  $sqlconmar = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'CONFIRMATION' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 3";
                  $sqlmatmar = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'WEDDING' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 3";
                  $sqlfunmar = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'FUNERAL MASS' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 3";
                  $sqlmasmar = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'MASS INTENTION' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 3";
                  $sqlblesmar = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BLESSING' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 3";
                  $connbapmar = $conn->query($sqlbapmar);
                  $connconmar = $conn->query($sqlconmar);
                  $connmatmar = $conn->query($sqlmatmar);
                  $connfunmar = $conn->query($sqlfunmar);
                  $connmasmar = $conn->query($sqlmasmar);
                  $connblesmar = $conn->query($sqlblesmar);
                  $countbapmar = mysqli_num_rows($connbapmar);
                  $countconmar = mysqli_num_rows($connconmar);
                  $countmatmar = mysqli_num_rows($connmatmar);
                  $countfunmar = mysqli_num_rows($connfunmar);
                  $countmasmar = mysqli_num_rows($connmasmar);
                  $countblesmar = mysqli_num_rows($connblesmar);

                  $sqlbapapr = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BAPTISM' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 4";
                  $sqlconapr = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'CONFIRMATION' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 4";
                  $sqlmatapr = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'WEDDING' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 4";
                  $sqlfunapr = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'FUNERAL MASS' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 4";
                  $sqlmasapr = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'MASS INTENTION' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 4";
                  $sqlblesapr = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BLESSING' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 4";
                  $connbapapr = $conn->query($sqlbapapr);
                  $connconapr = $conn->query($sqlconapr);
                  $connmatapr = $conn->query($sqlmatapr);
                  $connfunapr = $conn->query($sqlfunapr);
                  $connmasapr = $conn->query($sqlmasapr);
                  $connblesapr = $conn->query($sqlblesapr);
                  $countbapapr = mysqli_num_rows($connbapapr);
                  $countconapr = mysqli_num_rows($connconapr);
                  $countmatapr = mysqli_num_rows($connmatapr);
                  $countfunapr = mysqli_num_rows($connfunapr);
                  $countmasapr = mysqli_num_rows($connmasapr);
                  $countblesapr = mysqli_num_rows($connblesapr);

                  $sqlbapmay = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BAPTISM' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 5";
                  $sqlconmay = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'CONFIRMATION' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 5";
                  $sqlmatmay = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'WEDDING' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 5";
                  $sqlfunmay = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'FUNERAL MASS' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 5";
                  $sqlmasmay = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'MASS INTENTION' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 5";
                  $sqlblesmay = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BLESSING' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 5";
                  $connbapmay = $conn->query($sqlbapmay);
                  $connconmay = $conn->query($sqlconmay);
                  $connmatmay = $conn->query($sqlmatmay);
                  $connfunmay = $conn->query($sqlfunmay);
                  $connmasmay = $conn->query($sqlmasmay);
                  $connblesmay = $conn->query($sqlblesmay);
                  $countbapmay = mysqli_num_rows($connbapmay);
                  $countconmay = mysqli_num_rows($connconmay);
                  $countmatmay = mysqli_num_rows($connmatmay);
                  $countfunmay = mysqli_num_rows($connfunmay);
                  $countmasmay = mysqli_num_rows($connmasmay);
                  $countblesmay = mysqli_num_rows($connblesmay);

                  $sqlbapjun = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BAPTISM' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 6";
                  $sqlconjun = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'CONFIRMATION' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 6";
                  $sqlmatjun = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'WEDDING' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 6";
                  $sqlfunjun = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'FUNERAL MASS' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 6";
                  $sqlmasjun = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'MASS INTENTION' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 6";
                  $sqlblesjun = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BLESSING' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 6";
                  $connbapjun = $conn->query($sqlbapjun);
                  $connconjun = $conn->query($sqlconjun);
                  $connmatjun = $conn->query($sqlmatjun);
                  $connfunjun = $conn->query($sqlfunjun);
                  $connmasjun = $conn->query($sqlmasjun);
                  $connblesjun = $conn->query($sqlblesjun);
                  $countbapjun = mysqli_num_rows($connbapjun);
                  $countconjun = mysqli_num_rows($connconjun);
                  $countmatjun = mysqli_num_rows($connmatjun);
                  $countfunjun = mysqli_num_rows($connfunjun);
                  $countmasjun = mysqli_num_rows($connmasjun);
                  $countblesjun = mysqli_num_rows($connblesjun);

                  $sqlbapjul = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BAPTISM' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 7";
                  $sqlconjul = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'CONFIRMATION' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 7";
                  $sqlmatjul = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'WEDDING' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 7";
                  $sqlfunjul = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'FUNERAL MASS' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 7";
                  $sqlmasjul = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'MASS INTENTION' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 7";
                  $sqlblesjul = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BLESSING' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 7";
                  $connbapjul = $conn->query($sqlbapjul);
                  $connconjul = $conn->query($sqlconjul);
                  $connmatjul = $conn->query($sqlmatjul);
                  $connfunjul = $conn->query($sqlfunjul);
                  $connmasjul = $conn->query($sqlmasjul);
                  $connblesjul = $conn->query($sqlblesjul);
                  $countbapjul = mysqli_num_rows($connbapjul);
                  $countconjul = mysqli_num_rows($connconjul);
                  $countmatjul = mysqli_num_rows($connmatjul);
                  $countfunjul = mysqli_num_rows($connfunjul);
                  $countmasjul = mysqli_num_rows($connmasjul);
                  $countblesjul = mysqli_num_rows($connblesjul);

                  $sqlbapaug = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BAPTISM' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 8";
                  $sqlconaug = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'CONFIRMATION' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 8";
                  $sqlmataug = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'WEDDING' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 8";
                  $sqlfunaug = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'FUNERAL MASS' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 8";
                  $sqlmasaug = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'MASS INTENTION' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 8";
                  $sqlblesaug = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BLESSING' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 8";
                  $connbapaug = $conn->query($sqlbapaug);
                  $connconaug = $conn->query($sqlconaug);
                  $connmataug = $conn->query($sqlmataug);
                  $connfunaug = $conn->query($sqlfunaug);
                  $connmasaug = $conn->query($sqlmasaug);
                  $connblesaug = $conn->query($sqlblesaug);
                  $countbapaug = mysqli_num_rows($connbapaug);
                  $countconaug = mysqli_num_rows($connconaug);
                  $countmataug = mysqli_num_rows($connmataug);
                  $countfunaug = mysqli_num_rows($connfunaug);
                  $countmasaug = mysqli_num_rows($connmasaug);
                  $countblesaug = mysqli_num_rows($connblesaug);

                  $sqlbapsep = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BAPTISM' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 9";
                  $sqlconsep = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'CONFIRMATION' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 9";
                  $sqlmatsep = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'WEDDING' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 9";
                  $sqlfunsep = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'FUNERAL MASS' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 9";
                  $sqlmassep = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'MASS INTENTION' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 9";
                  $sqlblessep = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BLESSING' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 9";
                  $connbapsep = $conn->query($sqlbapsep);
                  $connconsep = $conn->query($sqlconsep);
                  $connmatsep = $conn->query($sqlmatsep);
                  $connfunsep = $conn->query($sqlfunsep);
                  $connmassep = $conn->query($sqlmassep);
                  $connblessep = $conn->query($sqlblessep);
                  $countbapsep = mysqli_num_rows($connbapsep);
                  $countconsep = mysqli_num_rows($connconsep);
                  $countmatsep = mysqli_num_rows($connmatsep);
                  $countfunsep = mysqli_num_rows($connfunsep);
                  $countmassep = mysqli_num_rows($connmassep);
                  $countblessep = mysqli_num_rows($connblessep);

                  $sqlbapoct = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BAPTISM' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 10";
                  $sqlconoct = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'CONFIRMATION' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 10";
                  $sqlmatoct = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'WEDDING' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 10";
                  $sqlfunoct = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'FUNERAL MASS' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 10";
                  $sqlmasoct = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'MASS INTENTION' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 10";
                  $sqlblesoct = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BLESSING' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 10";
                  $connbapoct = $conn->query($sqlbapoct);
                  $connconoct = $conn->query($sqlconoct);
                  $connmatoct = $conn->query($sqlmatoct);
                  $connfunoct = $conn->query($sqlfunoct);
                  $connmasoct = $conn->query($sqlmasoct);
                  $connblesoct = $conn->query($sqlblesoct);
                  $countbapoct = mysqli_num_rows($connbapoct);
                  $countconoct = mysqli_num_rows($connconoct);
                  $countmatoct = mysqli_num_rows($connmatoct);
                  $countfunoct = mysqli_num_rows($connfunoct);
                  $countmasoct = mysqli_num_rows($connmasoct);
                  $countblesoct = mysqli_num_rows($connblesoct);

                  $sqlbapnov = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BAPTISM' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 11";
                  $sqlconnov = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'CONFIRMATION' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 11";
                  $sqlmatnov = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'WEDDING' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 11";
                  $sqlfunnov = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'FUNERAL MASS' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 11";
                  $sqlmasnov = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'MASS INTENTION' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 11";
                  $sqlblesnov = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BLESSING' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 11";
                  $connbapnov = $conn->query($sqlbapnov);
                  $connconnov = $conn->query($sqlconnov);
                  $connmatnov = $conn->query($sqlmatnov);
                  $connfunnov = $conn->query($sqlfunnov);
                  $connmasnov = $conn->query($sqlmasnov);
                  $connblesnov = $conn->query($sqlblesnov);
                  $countbapnov = mysqli_num_rows($connbapnov);
                  $countconnov = mysqli_num_rows($connconnov);
                  $countmatnov = mysqli_num_rows($connmatnov);
                  $countfunnov = mysqli_num_rows($connfunnov);
                  $countmasnov = mysqli_num_rows($connmasnov);
                  $countblesnov = mysqli_num_rows($connblesnov);

                  $sqlbapdec = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BAPTISM' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 12";
                  $sqlcondec = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'CONFIRMATION' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 12";
                  $sqlmatdec = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'WEDDING' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 12";
                  $sqlfundec = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'FUNERAL MASS' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 12";
                  $sqlmasdec = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'MASS INTENTION' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 12";
                  $sqlblesdec = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BLESSING' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = 12";
                  $connbapdec = $conn->query($sqlbapdec);
                  $conncondec = $conn->query($sqlcondec);
                  $connmatdec = $conn->query($sqlmatdec);
                  $connfundec = $conn->query($sqlfundec);
                  $connmasdec = $conn->query($sqlmasdec);
                  $connblesdec = $conn->query($sqlblesdec);
                  $countbapdec = mysqli_num_rows($connbapdec);
                  $countcondec = mysqli_num_rows($conncondec);
                  $countmatdec = mysqli_num_rows($connmatdec);
                  $countfundec = mysqli_num_rows($connfundec);
                  $countmasdec = mysqli_num_rows($connmasdec);
                  $countblesdec = mysqli_num_rows($connblesdec);

                  $currmonth = date("m");
                  $sqlmonthbap = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BAPTISM' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = '$currmonth'";
                  $sqlmonthcon = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'CONFIRMATION' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = '$currmonth'";
                  $sqlmonthmat = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'WEDDING' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = '$currmonth'";
                  $sqlmonthfun = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'FUNERAL MASS' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = '$currmonth'";
                  $sqlmonthmas = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'MASS INTENTION' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = '$currmonth'";
                  $sqlmonthbles = "SELECT * FROM `reservationtbl` WHERE `strTypeofService` = 'BLESSING' AND `strReserveStatus` = 'CONFIRMED' AND MONTH(`dtReserve_Date`) = '$currmonth'";
                  $connbapmonth = $conn->query($sqlmonthbap);
                  $connconmonth = $conn->query($sqlmonthcon);
                  $connmatmonth = $conn->query($sqlmonthmat);
                  $connfunmonth = $conn->query($sqlmonthfun);
                  $connmasmonth = $conn->query($sqlmonthmas);
                  $connblesmonth = $conn->query($sqlmonthbles);
                  $countbapmonth = mysqli_num_rows($connbapmonth);
                  $countconmonth = mysqli_num_rows($connconmonth);
                  $countmatmonth = mysqli_num_rows($connmatmonth);
                  $countfunmonth = mysqli_num_rows($connfunmonth);
                  $countmasmonth = mysqli_num_rows($connmasmonth);
                  $countblesmonth = mysqli_num_rows($connblesmonth);

                ?>
                <?php
                  $curryear = date("Y");
                  $sqlbaptism = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BAPTISM' AND `strReserveStatus` = 'CONFIRMED' AND YEAR(`dtReserve_Date`) = '$curryear'";

                      $bconnstatus = $conn->query($sqlbaptism);
                      $totalbap = mysqli_num_rows($bconnstatus);
                    
                    echo "<div class='col-lg-3 col-xs-6'>";
                      echo "<div class='small-box bg-aqua'>";
                        echo "<div class='inner'>";
                          echo "<h3>$totalbap</h3>";

                          echo "<p>Baptism</p>";
                        echo "</div>";
                        echo "<div class='icon'>";
                          echo "<i class='fa fa-fw fa-child'></i>";
                        echo "</div>";
                        echo "<a href='#' class='small-box-footer'>More info <i class='fa fa-arrow-circle-right'></i></a>";
                      echo "</div>";
                    echo "</div>";
                    $sqlconfirm = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'CONFIRMATION' AND `strReserveStatus` = 'CONFIRMED' AND YEAR(`dtReserve_Date`) = '$curryear'";

                        $cconnstatus = $conn->query($sqlconfirm);
                        $totalconf = mysqli_num_rows($cconnstatus);

                      echo "<div class='col-lg-3 col-xs-6'>";
                        echo "<div class='small-box bg-green'>";
                          echo "<div class='inner'>";
                            echo "<h3>$totalconf</h3>";
                          
                            echo "<p>Confirmation</p>";
                          echo "</div>";
                          echo "<div class='icon'>";
                            echo "<i class='fa fa-fw fa-calendar-check-o'></i>";
                          echo "</div>";
                          echo "<a href='#' class='small-box-footer'>More info <i class='fa fa-arrow-circle-right'></i></a>";
                        echo "</div>";
                      echo "</div>";

                      $sqlmatrimony = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'WEDDING' AND `strReserveStatus` = 'CONFIRMED' AND YEAR(`dtReserve_Date`) = '$curryear'";

                        $matconnstatus = $conn->query($sqlmatrimony);
                        $totalmat = mysqli_num_rows($matconnstatus);

                      echo "<div class='col-lg-3 col-xs-6'>";
                        echo "<div class='small-box bg-yellow'>";
                          echo "<div class='inner'>";
                            echo "<h3>$totalmat</h3>";

                            echo "<p>Matrimony</p>";
                          echo "</div>";
                          echo "<div class='icon'>";
                            echo "<i class='fa fa-fw fa-venus-mars'></i>";
                          echo "</div>";
                          echo "<a href='#' class='small-box-footer'>More info <i class='fa fa-arrow-circle-right'></i></a>";
                        echo "</div>";
                      echo "</div>";

                      $sqlfun = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'FUNERAL MASS' AND `strReserveStatus` = 'CONFIRMED' AND YEAR(`dtReserve_Date`) = '$curryear'";

                        $fmconnstatus = $conn->query($sqlfun);
                        $totalfm = mysqli_num_rows($fmconnstatus);

                      echo "<div class='col-lg-3 col-xs-6'>";
                        echo "<div class='small-box bg-red'>";
                          echo "<div class='inner'>";
                            echo "<h3>$totalfm</h3>";

                            echo "<p>Funeral Mass</p>";
                          echo "</div>";
                          echo "<div class='icon'>";
                            echo "<i class='fa fa-fw fa-plus-square'></i>";
                          echo "</div>";
                          echo "<a href='#' class='small-box-footer'>More info <i class='fa fa-arrow-circle-right'></i></a>";
                        echo "</div>";
                      echo "</div>";

                      echo "<div class='col-lg-3 col-xs-6'>";
                        echo "<div class='small-box'>";
                         echo "</div>";
                      echo "</div>";

                      $sqlmas = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'MASS INTENTION' AND `strReserveStatus` = 'CONFIRMED' AND YEAR(`dtReserve_Date`) = '$curryear'";

                        $miconnstatus = $conn->query($sqlmas);
                        $totalmas = mysqli_num_rows($miconnstatus);

                      echo "<div class='col-lg-3 col-xs-6'>";          
                        echo "<div class='small-box bg-teal'>";
                          echo "<div class='inner'>";
                            echo "<h3>$totalmas</h3>";

                            echo "<p>Mass Intentions</p>";
                          echo "</div>";
                          echo "<div class='icon'>";
                            echo "<i class='fa fa-fw fa-bullhorn'></i>";
                          echo "</div>";
                          echo "<a href='#' class='small-box-footer'>More info <i class='fa fa-arrow-circle-right'></i></a>";
                        echo "</div>";
                      echo "</div>";

                     $sqlblessing = "SELECT `intReservationID`, `strTypeofService`, `strReserveStatus` FROM `reservationtbl` WHERE `strTypeofService` = 'BLESSING' AND `strReserveStatus` = 'CONFIRMED' AND YEAR(`dtReserve_Date`) = '$curryear'";

                        $bsconnstatus = $conn->query($sqlblessing);
                        $totalbs = mysqli_num_rows($bsconnstatus); 
                      
                      
                      $monthname = date("F", mktime(0, 0, 0, $currmonth, 10));

                      echo "<div class='col-lg-3 col-xs-6'>";
                        echo "<div class='small-box bg-primary'>";
                          echo "<div class='inner'>";
                            echo "<h3>$totalbs</h3>";

                            echo "<p>Blessings</p>";
                          echo "</div>";
                          echo "<div class='icon'>";
                            echo "<i class='fa fa-fw fa-file-text'></i>";
                          echo "</div>";
                          echo "<a href='#' class='small-box-footer'>More info <i class='fa fa-arrow-circle-right'></i></a>";
                        echo "</div>";

                        echo "<div class='col-lg-3 col-xs-6'>";
                        echo "<div class='small-box'>";
                         echo "</div>";
                      echo "</div>";
                    ?>
              </div>
            </div>

          <div class="row">
            <div class="col-md-9">
              <p class="text-center">
                <strong>Confirmed reservations: Year- <?php echo $curryear ?></strong>
             </p>

            <div class="chart">
            <canvas id="linechart" style="height: 180px;"></canvas>
            </div>
            </div>
                <div class="col-md-3">
                  <div class="box box-solid box-primary">
                    <div class="box-header  with-border">
                        <h3 class="box-title">Services</h3>
                    </div>
                  <div class="box-body">
                  <ul class="chart-legend" style="width: 110px;">
                    <li><i class="fa fa-circle-o text-aqua"></i> Baptism</li>
                    <li><i class="fa fa-circle-o text-green"></i> Confirmation</li>
                    <li><i class="fa fa-circle-o text-yellow"></i> Matrimony</li>
                    <li><i class="fa fa-circle-o text-red"></i> Funeral Mass</li>
                    <li><i class="fa fa-circle-o text-teal"></i> Mass Intentions</li>
                    <li><i class="fa fa-circle-o text-primary"></i> Blessings</li>
                  </ul>
                  </div>
                </div>       
                
             </div>
             
              </div>
            
             </div>
      </div>
        <?php
        echo "<div class='col-md-9'>";   
          echo "<div class='box box-primary' style='width: 540px; position: relative; left: -14px'>";
            echo "<div class='box-header with-border'>";
              echo "<h3 class='box-title fa fa-bell'> Reminder</h3>";
            echo "</div>";
          echo "<div class='box-body'>";
              echo "<div class='col-md-6'>";
              echo "<div class='box box-solid box-success' style='width: 540px; position: relative; left: -24px'>";
                echo "<div class='box-header with-border'>";
                  echo "<h3 class='box-title'>Upcoming</h3>";
                    echo "<div class='box-tools pull-right'>";
                      echo "<button class='btn btn-box-tool' data-widget='collapse'><i class='fa fa-minus'></i></button>";
                    echo "</div>";
                echo "</div>";
                echo "<div class='box-body'>";
                  echo "<div class='box box-solid'>";
                      echo "<div class='box-header'>";
                        echo "<h3 class='box-title'>On Church Services</h3>";
                        echo "<div class='box-tools pull-right'>";
                                echo "<button class='btn btn-box-tool' data-widget='collapse'><i class='fa fa-minus'></i></button>";
                              echo "</div>";
                    echo "</div>";
                    $csnow = date('Y-m-d');
                    $upcomecs = "SELECT * FROM eventstbl WHERE '$csnow' BETWEEN `start` -7 AND `start` AND `color` = '0071C5'";
                    $select_query_res = $conn->query($upcomecs);    
                      echo "<div class='box-body no-padding'>";
                        echo "<table class='table table-condensed'>";
                        if($select_query_res -> num_rows > 0) {
                          echo "<tr>";
                            echo "<th>Service</th>";
                            echo "<th>Date</th>";
                            echo "<th>Time</th>";
                            echo "<th></th>";
                          echo "</tr>";
                          while($csrow =  $select_query_res->fetch_assoc()) {
                              $csparent = strtotime($csrow['start']);
                              $cschild = date('m.j.Y',$csparent);
                              $cschild2 = date('H:i',$csparent);
                              $csdate1 = strtotime($csrow['start']);
                              $csstrdate = date('Y-m-d');
                              $csdate2 = strtotime($csstrdate);
                              $cssecs = $csdate1-$csdate2;
                              $csdays = $cssecs/86400;
                              $rndcsdays = round($csdays);
                          if ($csdays < 7){    
                          echo "<tr>";
                            echo "<td>",$csrow['title'],"</td>";
                            echo "<td>",$cschild,"</td>";
                            echo "<td>",$cschild2,"</td>";
                            echo "<td style='font-size: 12.5px'><i>$rndcsdays days left</i></td>";
                          echo "</tr>";
                                          }
                                            }
                        } 
                         else {
                  echo"<h4><i class='fa fa-info'></i> Note:</h4>";
                    echo "<i>No Upcoming Church services events</i>";
                  }
                      echo "</table>";
                    echo "</div>";
                  echo "</div>";

                echo "<div class='box box-solid' style='position: relative; top: -20px;'>";
                    echo "<div class='box-header'>";
                      echo "<h3 class='box-title'>Events</h3>";
                      echo "<div class='box-tools pull-right'>";
                              echo "<button class='btn btn-box-tool' data-widget='collapse'><i class='fa fa-minus'></i></button>";
                      echo "</div>";
                    echo "</div>";
                    $now = date('Y-m-d');
                    $upcomevn = "SELECT * FROM eventstbl WHERE '$now' BETWEEN `start` -7 AND `start` AND `color` != '0071C5'";
                     $select_query_result = $conn->query($upcomevn);    
                    echo "<div class='box-body no-padding'>";
                    echo "<table class='table table-condensed'>";
                      if($select_query_result -> num_rows > 0) { 
                        echo "<tr>";
                        echo "<th>Event</th>";
                        echo "<th>Date</th>";
                        echo "<th></th>";
                        echo "</tr>";
                      while($eventrow =  $select_query_result->fetch_assoc()) {
                              $date1 = strtotime($eventrow['start']);
                              $strdate = date('Y-m-d');
                              $date2 = strtotime($strdate);
                              $secs = $date1-$date2;
                              $days = $secs/86400;
                          if ($days < 7){      
                          echo "<tr>";
                            echo "<td>",$eventrow['title'],"</td>";
                            echo "<td>",$eventrow['start'],"</td>";
                            echo "<td style='font-size: 12.5px'><i>$days days left</i></td>";
                          echo "</tr>";
                                        }
                          }
                      }
                      else {
                  echo"<h4><i class='fa fa-info'></i> Note:</h4>";
                    echo "<i>No Upcoming events</i>";
                  }

                      echo "</table>";
                  echo "</div>";   


                echo "</div>";
              echo "</div>";
            echo "</div>";
         
 
            echo "<div class='col-md-6'>";
              echo "<div class='box box-solid box-danger' style='width: 540px; position: relative; top: -20px; left: -40px'>";
                echo "<div class='box-header with-border'>";
                  echo "<h3 class='box-title'>Unconfirmed Reservations</h3>";
                    echo "<div class='box-tools pull-right'>";
                      echo "<button class='btn btn-box-tool' data-widget='collapse'><i class='fa fa-minus'></i></button>";
                    echo "</div>";
                echo "</div>";
                    $urnow = date('Y-m-d');
                    $urcome = "SELECT * FROM reservationtbl WHERE '$urnow' < `dtmDate_Reserved` AND `strReserveStatus` = 'PENDING'";
                     $select_query_ur = $conn->query($urcome);
                    echo "<div class='box-body'>";
                    echo "<table class='table'>";
                     if($select_query_ur -> num_rows > 0) { 
                   echo "<tr>";
                  echo "<th>Username</th>";
                  echo "<th>Service</th>";
                  echo "<th>Date</th>";
                  echo "<th></th>";
                echo "</tr>";
                while($ucreservrow =  $select_query_ur->fetch_assoc()) {
                  $urdate1 = strtotime($ucreservrow['dtmDate_Reserved']);
                  $urstrdate = date('Y-m-d');
                  $urdate2 = strtotime($urstrdate);
                  $ursecs = $urdate1-$urdate2;
                  $urdays = $ursecs/86400;
                  if ($urdays < 7){      
                echo "<tr>";
                  echo "<td>",$ucreservrow['intReservationID'],"</td>";
                  echo "<td>",$ucreservrow['strTypeofService'],"</td>";
                  echo "<td>",$ucreservrow['dtmDate_Reserved'],"</td>";
                  echo "<td style='font-size: 12.5px'><i>$urdays days left</i></td>";
                echo "</tr>";
                }
                            }                            

        } 
         else {
                  echo"<h4><i class='fa fa-info'></i> Note:</h4>";
                    echo "<i>No Pending Requests</i>";
                  }
              echo "</table>";
                  echo "</div>";
                echo "</div>";
               echo "</div>"; 
            echo "</div>";
          echo "</div>";
          echo "</div>";
      echo "</div>";
      ?>
      <div class="col-md-6" style=" position: relative; left: 540px; top: -518px;">
          <!-- Custom Tabs (Pulled to the right) -->
          <div class="box box-danger" style="width: 533px; height: 340px">
            <div class="box-header with-border">
              <h3 class="box-title">Confirmed Reservation: Month of <?php echo $monthname ?></h3>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">
           
                  <div class="chart-responsive" style= "width: 463px; position: relative;left: -70px">
                    <canvas id="pieChart2"></canvas>
                  </div>
                   <div class="col-md-4" style= "width: 180px; position: relative;right: -340px; top: -100px">
                  <ul class="chart-legend">
                    <li><i class="fa fa-circle-o text-aqua"></i>Baptism:&emsp;&emsp;&emsp;&emsp;<?php echo $countbapmonth?></li>
                    <li><i class="fa fa-circle-o text-green"></i> Confirmation:&emsp;&ensp;&nbsp;<?php echo $countconmonth?></li>
                    <li><i class="fa fa-circle-o text-yellow"></i> Matrimony:&emsp;&emsp;&ensp;&nbsp;<?php echo $countmatmonth?></li>
                    <li><i class="fa fa-circle-o text-red"></i> Funeral Mass:&emsp;&ensp;&nbsp;&nbsp;<?php echo $countfunmonth?></li>
                    <li><i class="fa fa-circle-o text-teal"></i> Mass Intentions:&ensp;&nbsp;<?php echo $countmasmonth?></li>
                    <li><i class="fa fa-circle-o text-primary"></i> Blessings:&emsp;&emsp;&ensp;&ensp;&nbsp;&nbsp;<?php echo $countblesmonth?></li>
                  </ul>
                  </div>
                  </div>
                </div>
              </div>
              </div>
          
      <?php
      $dt = date("Y-m-d");
      $weeknum = date('W',strtotime($dt))-1;
      
      $currentgospel = "SELECT `strGospel_Content` FROM `gospeltbl` WHERE `intGWID` = '$weeknum'";
      $select_gospel = $conn->query($currentgospel);
                  if($select_gospel -> num_rows > 0) {
                    while($gospel = $select_gospel->fetch_assoc()) {
                    $content = $gospel['strGospel_Content'];
                    $string = strlen($content)>550?  substr($content,0,550).".......":$content;
     
      echo "<div class='col-md-3' style='position: relative; right: 370px;'>";
      echo "<div class='box box-solid box-primary' style='width: 533px;'>";
          echo "<div class='box-header'>";
            echo "<h3 class='box-title'>Gospel of the Week</h3>";
          echo "</div>";        
            echo "<div class='box-body'>";
              echo "<p>",nl2br($string),"</p>";
              echo "<button type='button' class='btn btn-default btn-xs pull-right' data-toggle='modal' data-target='#modal-default'>";
                echo "View full text";
              echo "</button>";
            echo "</div>";       
           echo "</div>";
      echo "</div>";

      echo "<div class='modal modal-default fade' id='modal-default'>";
          echo "<div class='modal-dialog'>";
            echo "<div class='modal-content'>";
              echo "<div class='modal-header'>";
                echo "<button type='button' class='close' data-dismiss='modal' aria-label='Close'>";
                  echo "<span aria-hidden='true'>&times;</span></button>";
                echo "<h4 class='modal-title'></h4>";
              echo "</div>";
              echo "<div class='modal-body'>";
                echo "<p>",nl2br($content),"</p>";
              echo "</div>";
              echo "<div class='modal-footer'>";
                echo "<button type='button' class='btn btn-default pull-right' data-dismiss='modal'>Close</button>";
              echo "</div>";
            echo "</div>";
        echo "</div>";
    }
  }

      ?>

    
   
    </div>
    </div>
    </section> 
    </div>

<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<script src="dist/js/adminlte.min.js"></script>
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="bower_components/chart.js/Chart.js"></script>
<script src="dist/js/pages/dashboard2.js"></script>
<script src="dist/js/demo.js"></script>

<script>
    var baptotaljan = function(){ return <?php echo $countbapjan?>};
    var baptotalfeb = function(){ return <?php echo $countbapfeb?>};
    var baptotalmar = function(){ return <?php echo $countbapmar?>};
    var baptotalapr = function(){ return <?php echo $countbapapr?>};
    var baptotalmay = function(){ return <?php echo $countbapmay?>}; 
    var baptotaljun = function(){ return <?php echo $countbapjun?>};
    var baptotaljul = function(){ return <?php echo $countbapjul?>};
    var baptotalaug = function(){ return <?php echo $countbapaug?>};
    var baptotalsep = function(){ return <?php echo $countbapsep?>};
    var baptotaloct = function(){ return <?php echo $countbapoct?>};
    var baptotalnov = function(){ return <?php echo $countbapnov?>};
    var baptotaldec = function(){ return <?php echo $countbapdec?>};

    var contotaljan = function(){ return <?php echo $countconjan?>};
    var contotalfeb = function(){ return <?php echo $countconfeb?>};
    var contotalmar = function(){ return <?php echo $countconmar?>};
    var contotalapr = function(){ return <?php echo $countconapr?>};
    var contotalmay = function(){ return <?php echo $countconmay?>}; 
    var contotaljun = function(){ return <?php echo $countconjun?>};
    var contotaljul = function(){ return <?php echo $countconjul?>};
    var contotalaug = function(){ return <?php echo $countconaug?>};
    var contotalsep = function(){ return <?php echo $countconsep?>};
    var contotaloct = function(){ return <?php echo $countconoct?>};
    var contotalnov = function(){ return <?php echo $countconnov?>};
    var contotaldec = function(){ return <?php echo $countcondec?>};

    var mattotaljan = function(){ return <?php echo $countmatjan?>};
    var mattotalfeb = function(){ return <?php echo $countmatfeb?>};
    var mattotalmar = function(){ return <?php echo $countmatmar?>};
    var mattotalapr = function(){ return <?php echo $countmatapr?>};
    var mattotalmay = function(){ return <?php echo $countmatmay?>}; 
    var mattotaljun = function(){ return <?php echo $countmatjun?>};
    var mattotaljul = function(){ return <?php echo $countmatjul?>};
    var mattotalaug = function(){ return <?php echo $countmataug?>};
    var mattotalsep = function(){ return <?php echo $countmatsep?>};
    var mattotaloct = function(){ return <?php echo $countmatoct?>};
    var mattotalnov = function(){ return <?php echo $countmatnov?>};
    var mattotaldec = function(){ return <?php echo $countmatdec?>};

    var funtotaljan = function(){ return <?php echo $countfunjan?>};
    var funtotalfeb = function(){ return <?php echo $countfunfeb?>};
    var funtotalmar = function(){ return <?php echo $countfunmar?>};
    var funtotalapr = function(){ return <?php echo $countfunapr?>};
    var funtotalmay = function(){ return <?php echo $countfunmay?>}; 
    var funtotaljun = function(){ return <?php echo $countfunjun?>};
    var funtotaljul = function(){ return <?php echo $countfunjul?>};
    var funtotalaug = function(){ return <?php echo $countfunaug?>};
    var funtotalsep = function(){ return <?php echo $countfunsep?>};
    var funtotaloct = function(){ return <?php echo $countfunoct?>};
    var funtotalnov = function(){ return <?php echo $countfunnov?>};
    var funtotaldec = function(){ return <?php echo $countfundec?>};

    var mastotaljan = function(){ return <?php echo $countmasjan?>};
    var mastotalfeb = function(){ return <?php echo $countmasfeb?>};
    var mastotalmar = function(){ return <?php echo $countmasmar?>};
    var mastotalapr = function(){ return <?php echo $countmasapr?>};
    var mastotalmay = function(){ return <?php echo $countmasmay?>}; 
    var mastotaljun = function(){ return <?php echo $countmasjun?>};
    var mastotaljul = function(){ return <?php echo $countmasjul?>};
    var mastotalaug = function(){ return <?php echo $countmasaug?>};
    var mastotalsep = function(){ return <?php echo $countmassep?>};
    var mastotaloct = function(){ return <?php echo $countmasoct?>};
    var mastotalnov = function(){ return <?php echo $countmasnov?>};
    var mastotaldec = function(){ return <?php echo $countmasdec?>};

    var blestotaljan = function(){ return <?php echo $countblesjan?>};
    var blestotalfeb = function(){ return <?php echo $countblesfeb?>};
    var blestotalmar = function(){ return <?php echo $countblesmar?>};
    var blestotalapr = function(){ return <?php echo $countblesapr?>};
    var blestotalmay = function(){ return <?php echo $countblesmay?>}; 
    var blestotaljun = function(){ return <?php echo $countblesjun?>};
    var blestotaljul = function(){ return <?php echo $countblesjul?>};
    var blestotalaug = function(){ return <?php echo $countblesaug?>};
    var blestotalsep = function(){ return <?php echo $countblessep?>};
    var blestotaloct = function(){ return <?php echo $countblesoct?>};
    var blestotalnov = function(){ return <?php echo $countblesnov?>};
    var blestotaldec = function(){ return <?php echo $countblesdec?>};
    
    
    var lineChartData = {
      labels : ["January","February","March","April","May","June","July","August","September","October","November","December"],
      datasets : [
        {
          label: "Baptism",
          fillColor : "rgba(0,192,239,0)",
          strokeColor : "rgba(0,192,239,1)",
          pointColor : "rgba(0,192,239,1)",
          pointStrokeColor : "#fff",
          pointHighlightFill : "#fff",
          pointHighlightStroke : "rgba(0,192,239,1)",
          data : [baptotaljan(),baptotalfeb(),baptotalmar(),baptotalapr(),baptotalmay(),baptotaljun(),baptotaljul(),baptotalaug(),baptotalsep(),baptotaloct(),baptotalnov(),baptotaldec()]
        },
        {
          label: "Confirmation",
          fillColor : "rgba(0,166,90,0)",
          strokeColor : "rgba(0,166,90,1)",
          pointColor : "rgba(0,166,90,1)",
          pointStrokeColor : "#fff",
          pointHighlightFill : "#fff",
          pointHighlightStroke : "rgba(0,166,90,1)",
          data : [contotaljan(),contotalfeb(),contotalmar(),contotalapr(),contotalmay(),contotaljun(),contotaljul(),contotalaug(),contotalsep(),contotaloct(),contotalnov(),contotaldec()]
        },
        {
          label: "Matrimony",
          fillColor : "rgba(243,156,18,0)",
          strokeColor : "rgba(243,156,18,1)",
          pointColor : "rgba(243,156,18,1)",
          pointStrokeColor : "#fff",
          pointHighlightFill : "#fff",
          pointHighlightStroke : "rgba(243,156,18,1)",
          data : [mattotaljan(),mattotalfeb(),mattotalmar(),mattotalapr(),mattotalmay(),mattotaljun(),mattotaljul(),mattotalaug(),mattotalsep(),mattotaloct(),mattotalnov(),mattotaldec()]
        },
        {
          label: "Funeral Mass",
          fillColor : "rgba(245,105,84,0)",
          strokeColor : "rgba(245,105,84,1)",
          pointColor : "rgba(245,105,84,1)",
          pointStrokeColor : "#fff",
          pointHighlightFill : "#fff",
          pointHighlightStroke : "rgba(151,187,205,1)",
          data : [funtotaljan(),funtotalfeb(),baptotalmar(),funtotalapr(),funtotalmay(),funtotaljun(),funtotaljul(),funtotalaug(),funtotalsep(),funtotaloct(),funtotalnov(),funtotaldec()]
        },
         {
          label: "Mass Intentions",
          fillColor : "rgba(57,204,204,0)",
          strokeColor : "rgba(57,204,204,1)",
          pointColor : "rgba(57,204,204,1)",
          pointStrokeColor : "#fff",
          pointHighlightFill : "#fff",
          pointHighlightStroke : "rgba(57,204,204,1)",
          data : [mastotaljan(),mastotalfeb(),mastotalmar(),mastotalapr(),mastotalmay(),mastotaljun(),mastotaljul(),mastotalaug(),mastotalsep(),mastotaloct(),mastotalnov(),mastotaldec()]
        },
        {
          label: "Blessings",
          fillColor : "rgba(60,141,188,0)",
          strokeColor : "rgba(60,141,188,1)",
          pointColor : "rgba(60,141,188,1)",
          pointStrokeColor : "#fff",
          pointHighlightFill : "#fff",
          pointHighlightStroke : "rgba(60,141,188,1)",
          data : [blestotaljan(),blestotalfeb(),blestotalmar(),blestotalapr(),blestotalmay(),blestotaljun(),blestotaljul(),blestotalaug(),blestotalsep(),blestotaloct(),blestotalnov(),blestotaldec()]
        },
      ]
    }

  window.onload = function(){
    var ctx = document.getElementById("linechart").getContext("2d");
    window.myLine = new Chart(ctx).Line(lineChartData, {
      responsive: true,
      bezierCurve : false

    });
  }
</script>

<script>

    $(function () {
    var baptotalmonth = function(){ return <?php echo $countbapmonth?>};
    var contotalmonth = function(){ return <?php echo $countconmonth?>};
    var mattotalmonth = function(){ return <?php echo $countmatmonth?>};
    var funtotalmonth = function(){ return <?php echo $countfunmonth?>};
    var mastotalmonth = function(){ return <?php echo $countmasmonth?>}; 
    var blestotalmonth = function(){ return <?php echo $countblesmonth?>};
      
    var pieChartCanvas = $('#pieChart2').get(0).getContext('2d')
    var pieChart2       = new Chart(pieChartCanvas)
    var PieData        = [
      {
        value    : baptotalmonth(),
        color    : '#00c0ef',
        highlight: '#00c0ef',
        label    : 'Baptism'
      },
      {
        value    : contotalmonth(),
        color    : '#00a65a',
        highlight: '#00a65a',
        label    : 'Confirmation'
      },
      {
        value    : mattotalmonth(),
        color    : '#f39c12',
        highlight: '#f39c12',
        label    : 'Matrimony'
      },
      {
        value    : funtotalmonth(),
        color    : '#f56954',
        highlight: '#f56954',
        label    : 'Funeral Mass'
      },
      {
        value    : mastotalmonth(),
        color    : '#39cccc',
        highlight: '#39cccc',
        label    : 'Mass Intentions'
      },
      {
        value    : blestotalmonth(),
        color    : '#3c8dbc',
        highlight: '#3c8dbc',
        label    : 'Blessings'
      }
    ]
    var pieOptions     = {
      segmentShowStroke    : true,
      segmentStrokeColor   : '#fff',
      segmentStrokeWidth   : 2,
      percentageInnerCutout: 50, 
      animationSteps       : 100,
      animationEasing      : 'easeOutBounce',
      animateRotate        : true,
      animateScale         : false,
      responsive           : true,
      maintainAspectRatio  : true,
      
    }
    pieChart2.Doughnut(PieData, pieOptions)
  
})
</script>
  
</body>
</html>
