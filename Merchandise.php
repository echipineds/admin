<?php
session_start();

include "DbConn.php";
$sqlpending = "SELECT intReserveCustomer_ID, strReserveStatus FROM reservationtbl WHERE reservationtbl.strReserveStatus = 'PENDING'";
$countpend = $conn->query($sqlpending);
$totalpending = mysqli_num_rows($countpend);

$sqlinbox = "SELECT * FROM messagetbl WHERE strMessage_ConvoCode = 'usersentunread'";
$countinbox = $conn->query($sqlinbox);  
$totalinbox = mysqli_num_rows($countinbox); 
?>

<?php 
if(isset($_SESSION['intUserID']) && !empty($_SESSION['intUserID'])) {
    if($_SESSION['intUserID'] != '1') {
      header ("Location: Signin.php");
    } 
    else {
    }
  }
  else {
    header ("Location: Signin.php");
  }
  ?>
<?php
      if(isset($_GET['s']) && $_GET['s'] == 'logout') {
      session_destroy();      
      if($conn) {
        $conn->close();
      }
      header("Location: " . $_SERVER['PHP_SELF']);      
      }
  ?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Merchandise</title>
  <link rel="icon" type="image/ico" href="OLALOGOmin.png " />
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <a href="index2.php" class="logo">
      <span class="logo-mini"><img src="OLALOGO.png" style="position: relative; left: -18px;" /></span>
      <span class="logo-lg"><img src="OLALOGO.png" style="position: relative; left: -18px;" /></span>
    </a>

    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><center>MAIN NAVIGATION</center></li>
        <li class="treeview">
          <a href="index2.php">
            <i class="fa fa-dashboard"></i><span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-list"></i> <span>Transaction</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <small class="label pull-right bg-yellow"><?php echo $totalpending?></small>
              <small class="label pull-right bg-green"></small>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="PendReserve.php"><i class="fa fa-fw fa-circle"></i> Pending Requests</a></li>
            <li><a href="ConfirmReserve.php"><i class="fa fa-fw fa-circle"></i> Confirmed Requests</a></li>
          </ul>
        </li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-fw fa-wrench"></i> <span>Maintenance</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="CalendarOfActivities.php"><i class="fa fa-calendar"></i> Calendar of Activities</a></li>
            <li><a href="Gospeloftheweek.php"><i class="fa fa-fw fa-file-text"></i> Gospel of the Week</a></li>
            <li><a href="Merchandise.php"><i class="fa fa-fw fa-shopping-cart"></i> Church Merchandise</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-users"></i> <span>Profiles</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="PriestProfile.php"><i class="fa fa-fw fa-user"></i>Priest</a></li>
            <li><a href="CustomerProfile.php"><i class="fa fa-fw fa-user"></i>Parishioners</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-file-text"></i> <span>Documents</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="AddDoc.php"><i class="fa fa-fw fa-files-o"></i>Certificates</a></li>
            <li><a href="#"><i class="fa fa-fw fa-files-o"></i>Vouchers</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-envelope"></i> <span>Mailbox</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <small class="label pull-right bg-blue"><?php echo $totalinbox?></small>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="MessageInbox.php"><i class="fa fa-fw fa-circle"></i>Inbox</a></li>
            <li><a href="MessageSent.php"><i class="fa fa-fw fa-circle"></i>Sent Items</a></li>
            <li><a href="MessageCompose.php"><i class="fa fa-fw fa-circle"></i>Compose message</a></li>
          </ul>
        </li>
        <li class="treeview-active">
          <a href="?s=logout">
            <i class="fa fa-sign-out"></i><span>Log out</span>
          </a>
        </li>
    </section>

  </aside>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Church Merchandises
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-fw fa-users"></i>Maintenance</a></li>
        <li class="active">Church Merchandise</li>
      </ol>
    </section>

     
    <section class='content'>
    <?php
      echo "<div class='row'>";
        echo "<div class='col-md-8'>";
          echo "<div class='box box-info' style='width:1100px;'>";
            echo "<div class='box-header with-border' style='height: 20px;'>";
            echo "</div>";
        
          $select_all_merch = "SELECT * FROM merchandise_tbl";
          $select_query_result = $conn->query($select_all_merch);
          if($select_query_result -> num_rows > 0) {
            echo "<div class='box-body'>";
              echo "<div>";
                echo "<table class='table no-margin'>";
                  echo "<thead>";
                  echo "<tr>";
                    echo "<th>Item</th>";
                    echo "<th>Price</th>";
                    echo "<th><center>Action</center></th>";
                  echo "</tr>";
                  echo "</thead>";
                  echo "<tbody>";
                  echo "<tr>";
                  while($merchtbl = $select_query_result->fetch_assoc()) {

                    echo "<td>",$merchtbl['strMerchandise_Name'],"</td>";
                    echo "<td>",$merchtbl['intMerchandise_Price'],"</td>";
                    echo "<td>";
                    echo "<form role='form' method='post' action='DeleteMerch.php'>";
                      echo "<div>";
                        echo "<center>";
                        echo "<button type='submit' data-toggle='tooltip' data-placement='top' name='edit' title='Edit' value='".$merchtbl['intMerchandise_ID']."' style='color: black; background-color: #337ab7; border-color: #2e6da4; border-radius: 50%;'>";
                          echo "<i class='fa fa-fw fa-edit'>";
                          echo "</i>";
                          echo "<a href='AddMerch.php'</a>";
                        echo "</button>";
                        echo "<button type='submit' href='EditMerch.php' data-toggle='tooltip' data-placement='top' name='delete' title='Delete' value='".$merchtbl['intMerchandise_ID']."' style='color: black; background-color: #337ab7; border-color: #2e6da4; border-radius: 50%;'>";
                          echo "<i class='fa fa-fw fa-trash-o'>";
                          echo "</i>";
                        echo "</button>";
                      echo "</center>";
                     echo "</div>";
                    echo "</form>"; 
                    echo "</td>";
                    echo "</tr>";


                  }
                      }
                    
                  else {
                    echo"<h4><i class='fa fa-info'></i> Note:</h4>";
                    echo "<i>No Merchandise Available</i>";
                  }

                      
                    
                  echo "</tbody>";

                echo "</table>";
                echo "<div class='footer pull-right' style=' -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px;'>";
                  echo "<a class='btn btn-primary' href='AddMerch.php' role='button'>+ Add Item</a>";
                 echo "</div>";  
              echo "</div>";              
            echo "</div>";
        echo "</div>";  
      echo "</div>";
    
  ?>
  </section>
</div>

<aside class="control-sidebar control-sidebar-dark">
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-gear"></i></a></li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane" id="control-sidebar-home-tab">
          <h3 class="control-sidebar-heading">Settings</h3>
          <ul class="control-sidebar-menu">
          <li>
            <a href="AdminProfile.php">
              <i class="menu-icon fa fa-user bg-yellow"></i>
              <div class="menu-info">
                <h4 class="control-sidebar-subheading"><p></p>Update Admin</h4>
              </div>
            </a>
          </li>
          <li>
            <a href="?s=logout">
              <i class="menu-icon fa fa-sign-out bg-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading"><p></p>Log out</h4>               
              </div>
            </a>
          </li>
        </ul>
     </div>
    </div>
  </aside>  
  
  <div class="control-sidebar-bg"></div>

</div>

<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<script src="dist/js/adminlte.min.js"></script>
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="bower_components/chart.js/Chart.js"></script>
<script src="dist/js/pages/dashboard2.js"></script>
<script src="dist/js/demo.js"></script>
</body>
</html>
