<?php
session_start();

include "DbConn.php";
$sqlpending = "SELECT intReserveCustomer_ID, strReserveStatus FROM reservationtbl WHERE reservationtbl.strReserveStatus = 'PENDING'";
$countpend = $conn->query($sqlpending);
$totalpending = mysqli_num_rows($countpend);

$sqlinbox = "SELECT * FROM messagetbl WHERE strMessage_ConvoCode = 'usersentunread'";
$countinbox = $conn->query($sqlinbox);  
$totalinbox = mysqli_num_rows($countinbox);
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Edit Priest Profile</title>
  <link rel="icon" type="image/ico" href="OLALOGOmin.png " />
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<?php
   if(isset($_POST['submit'])) {
  

    $priestid = $_POST['submit'];
    $fname = $_POST['strPriest_FirstName'];
    $lname = $_POST['strPriest_LastName'];
    $pos = $_POST['strPriest_Position'];
    $pic = addslashes(file_get_contents($_FILES['blobPriest_Photo']['tmp_name']));

    
    $update_priest = "UPDATE priesttbl SET strPriest_FirstName = '$fname', strPriest_LastName = '$lname', strPriest_Position = '$pos',blobPriest_Photo = '$pic' WHERE intPriestID = '$priestid'";
    $update_priest_query = $conn->query($update_priest);
     
    
  
  if($update_priest_query){
    $UpdatePriest_Msg = "<div class='alert alert-info text-center'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    Profile has been Updated!
    </div>";
    header("refresh:0.5 url=PriestProfile.php");
  }
  else{
    $UpdatePriest_Msg = "<div class='alert alert-info text-center'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    Failed to update Profile!
    </div>";
    header("refresh:0.5 url=PriestProfile.php");
  }

}

?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <a href="index2.php" class="logo">
      <span class="logo-mini"><img src="OLALOGO.png" style="position: relative; left: -18px;" /></span>
      <span class="logo-lg"><img src="OLALOGO.png" style="position: relative; left: -18px;" /></span>
    </a>

    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
    </nav>
  </header>

  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><center>MAIN NAVIGATION</center></li>
        <li class="treeview">
          <a href="index2.php">
            <i class="fa fa-dashboard"></i><span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-list"></i> <span>Transaction</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <small class="label pull-right bg-yellow"><?php echo $totalpending?></small>
              <small class="label pull-right bg-green"></small>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="PendReserve.php"><i class="fa fa-fw fa-circle"></i>Pending Requests</a></li>
            <li><a href="ConfirmReserve.php"><i class="fa fa-fw fa-circle"></i> Confirmed Requests</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-wrench"></i> <span>Maintenance</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="CalendarOfActivities.php"><i class="fa fa-calendar"></i>Calendar of Activities</a></li>
            <li><a href="Gospeloftheweek.php"><i class="fa fa-fw fa-file-text"></i>Gospel of the Week</a></li>
            <li><a href="Merchandise.php"><i class="fa fa-fw fa-shopping-cart"></i>Church Merchandise</a></li>
          </ul>
        </li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-fw fa-users"></i> <span>Profiles</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="PriestProfile.php"><i class="fa fa-fw fa-user"></i>Priest</a></li>
            <li><a href="CustomerProfile.php"><i class="fa fa-fw fa-user"></i>Parishioners</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-file-text"></i> <span>Documents</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="AddDoc.php"><i class="fa fa-fw fa-files-o"></i>Certificates</a></li>
            <li><a href="#"><i class="fa fa-fw fa-files-o"></i>Vouchers</a></li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-envelope"></i> <span>Mailbox</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <small class="label pull-right bg-blue"><?php echo $totalinbox?></small>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="MessageInbox.php"><i class="fa fa-fw fa-circle"></i>Inbox</a></li>
            <li><a href="MessageSent.php"><i class="fa fa-fw fa-circle"></i>Sent Items</a></li>
            <li><a href="MessageCompose.php"><i class="fa fa-fw fa-circle"></i>Compose message</a></li>
          </ul>
        </li>
        <li class="treeview-active">
          <a href="?s=logout">
            <i class="fa fa-sign-out"></i><span>Log out</span>
          </a>
        </li>
    </section>
  </aside>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Edit Priest Profile
      </h1>
      <ol class="breadcrumb">
        <li><i class="fa fa-fw fa-users"></i>Profiles</a></li>
        <li><a href="PriestProfile.php">Priest</a></li>
        <li class="active">Edit Priest Profile</li>
      </ol>
    </section>

   <section class="content">
      <div class="row">
        <div class="col-md-6">
          <div class="box box-primary" style="width: 970px;">
            <div class="box-header with-border" >             
            </div>
            <form role="form" method="post" action="EditPriestProfile.php" enctype="multipart/form-data">
              <?php
              echo @$UpdatePriest_Msg
              ?>
              <div class="box-body">
                <div class="form-group" style="width: 400px;">
                  <label>New First Name</label>
                  <input type="text" class="form-control" id="strPriest_FirstName" name="strPriest_FirstName" placeholder="Enter New First Name" required>
                </div>
                <div class="form-group" style="width: 400px;">
                  <label>New Last Name</label>
                  <input type="text" class="form-control" id="strPriest_LastName" name="strPriest_LastName" placeholder= "Enter new Last Name" required>
                </div>
                <div class="form-group" style="width: 400px;">
                  <label>New Position</label>
                  <select class="form-control" id="strPriest_Position" name="strPriest_Position" required>
                    <option name="strPriest_Position" id="strPriest_Position">Parish Priest</option>
                    <option name="strPriest_Position" id="strPriest_Position">Archpriest</option>
                    <option name="strPriest_Position" id="strPriest_Position">Monsignor</option>
                  </select>
                </div>
                
                <div class="form-group" style="width: 400px;">
                  <label>New Picture</label>
                  <input type="file" class="form-control" id="blobPriest_Photo" name="blobPriest_Photo" placeholder= "New Picture" required>
                </div>
                 <div class="footer pull-right" style=" -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px;">
                  <button type="submit" name="submit" value= "<?php echo $_SESSION['intPriestID'];?>" class="btn btn-success btn-sm" style="width: 80px;">
                    <i class="fa fa-check"></i>&nbspUpdate
                  </button>
                  <button type="reset" class="btn btn-danger btn-sm" style="width: 80px;">
                    <i class="fa fa-ban"></i> Clear
                  </button>
                </div>
               
              </div>
              <!-- /.box-body -->

              
            </form>
          </div>
        </section>
  </div>
  <!-- /.content-wrapper -->

 
  <div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap  -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS -->
<script src="bower_components/chart.js/Chart.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
