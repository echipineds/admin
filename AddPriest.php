<?php
  include('DbConn.php');
$sqlpending = "SELECT intReserveCustomer_ID, strReserveStatus FROM reservationtbl WHERE reservationtbl.strReserveStatus = 'PENDING'";
$countpend = $conn->query($sqlpending);
$totalpending = mysqli_num_rows($countpend);

$sqlinbox = "SELECT * FROM messagetbl WHERE strMessage_ConvoCode = 'usersentunread'";
$countinbox = $conn->query($sqlinbox);  
$totalinbox = mysqli_num_rows($countinbox);
  ?>


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Add Priest</title>
  <link rel="icon" type="image/ico" href="OLALOGOmin.png " />
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<?php
  
  if(isset($_POST['submit'])){
  $FName = mysqli_real_escape_string($conn,strip_tags($_POST['strPriest_FirstName']));
  $LName = mysqli_real_escape_string($conn,strip_tags($_POST['strPriest_LastName']));
  $Pos = mysqli_real_escape_string($conn,strip_tags($_POST['strPriest_Position']));
  $image = addslashes(file_get_contents($_FILES['blobPriest_Photo']['tmp_name']));
  $sql="INSERT INTO `priesttbl`(`strPriest_FirstName`,`strPriest_LastName`,`strPriest_Position`,`blobPriest_Photo`) VALUES ('$FName','$LName','$Pos','$image')";
  
  $result = mysqli_query($conn,$sql);

  if($result){
    $AddPriest_Msg = "<div class='alert alert-info text-center'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    Priest has been Added!
    </div>";
    header("refresh:0.5 url=PriestProfile.php");
  }
  else{
    $AddPriest_Msg = "<div class='alert alert-info text-center'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    Failed to add Priest!
    </div>";
  }
  }
?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper"> 

  <header class="main-header">
    <a href="index2.php" class="logo">
      <span class="logo-mini"><img src="OLALOGO.png" style="position: relative; left: -18px;" /></span>
      <span class="logo-lg"><img src="OLALOGO.png" style="position: relative; left: -18px;" /></span>
    </a>

    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
    </nav>
  </header>

  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><center>MAIN NAVIGATION</center></li>
        <li class="treeview">
          <a href="index2.php">
            <i class="fa fa-dashboard"></i><span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-list"></i> <span>Transaction</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <small class="label pull-right bg-yellow"><?php echo $totalpending?></small>
              <small class="label pull-right bg-green"></small>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="PendReserve.php"><i class="fa fa-fw fa-circle"></i>Pending Requests</a></li>
            <li><a href="ConfirmReserve.php"><i class="fa fa-fw fa-circle"></i> Confirmed Requests</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-wrench"></i> <span>Maintenance</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="CalendarOfActivities.php"><i class="fa fa-calendar"></i> Calendar of Activities</a></li>
            <li><a href="Gospeloftheweek.php"><i class="fa fa-fw fa-file-text"></i> Gospel of the Week</a></li>
            <li><a href="Merchandise.php"><i class="fa fa-fw fa-shopping-cart"></i> Church Merchandise</a></li>
          </ul>
        </li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-fw fa-users"></i> <span>Profiles</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="PriestProfile.php"><i class="fa fa-fw fa-user"></i>Priest</a></li>
            <li><a href="CustomerProfile.php"><i class="fa fa-fw fa-user"></i>Parishioners</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-file-text"></i> <span>Documents</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="AddDoc.php"><i class="fa fa-fw fa-files-o"></i>Certificates</a></li>
            <li><a href="#"><i class="fa fa-fw fa-files-o"></i>Vouchers</a></li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-envelope"></i> <span>Mailbox</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <small class="label pull-right bg-blue"><?php echo $totalinbox?></small>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="MessageInbox.php"><i class="fa fa-fw fa-circle"></i>Inbox</a></li>
            <li><a href="MessageSent.php"><i class="fa fa-fw fa-circle"></i>Sent Items</a></li>
            <li><a href="MessageCompose.php"><i class="fa fa-fw fa-circle"></i>Compose message</a></li>
          </ul>
        </li>
        <li class="treeview-active">
          <a href="?s=logout">
            <i class="fa fa-sign-out"></i><span>Log out</span>
          </a>
        </li>
    </section>
  </aside>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Add Priest
      </h1>
      <ol class="breadcrumb">
        <li><i class="fa fa-fw fa-users"></i> Profiles</a></li>
        <li><a href="PriestProfile.php">Priest Profile</a></li>
        <li class="active">Add Priest</li>
      </ol>
    </section>

   <section class="content">
      <div class="row">
        <div class="col-md-6">
          <div class="box box-primary" style="width:970px; height: 300px;">
            <div class="box-header with-border" >
             
            </div>
            <form role="form" method="post" action="AddPriest.php" enctype="multipart/form-data">
              <?php
              echo @$AddPriest_Msg
              ?>
              <div class="box-body">
                <div class="form-group" style="width: 400px;">
                  <label>First Name</label>
                  <input type="text" class="form-control" id="strPriest_FirstName" name="strPriest_FirstName" placeholder="First Name" required>
                </div>
                <div class="form-group" style="width: 400px; position: relative; left: 410px; top: -74px">
                  <label>Last Name</label>
                  <input type="text" class="form-control" id="strPriest_LastName" name="strPriest_LastName" placeholder="Last Name" required>
                </div>
                <div class="form-group" style="width: 400px;">
                  <label style="position: relative; top: -85px;">Position</label>
                  <select class="form-control" id="strPriest_Position" name="strPriest_Position" placeholder="Position" style="position: relative; top: -84px;" required>
                    <option value="Parish Priest" id="strPriest_Position" name="strPriest_Position" required>Parish Priest</option>
                    <option value="Archpriest" id="strPriest_Position" name="strPriest_Position" required>Archpriest</option>
                    <option value="Monsignor" id="strPriest_Position" name="strPriest_Position" required>Monsignor</option>
                  </select>
                </div>
                <input type="hidden" class="form-control" id="strPriest_Status" name="strPriest_Status" value="0" readonly="readonly" required>
                <div class="form-group" style="width: 400px;">
                  <label style="position: relative; top: -92px;">Select Photo</label>
                  <input type="file" class="form-control" id="blobPriest_Photo" name="blobPriest_Photo" accept="image/*" placeholder="Select Photo" style="position: relative; top: -90px;" required>
                </div>
                 <div class="footer pull-right" style=" -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; position: relative; top: -69px;">
                  <button type="submit" name="submit" class="btn btn-success btn-sm" style="width: 80px;">
                    <i class="fa fa-plus"></i>&nbspAdd
                  </button>
                  <button type="reset" class="btn btn-danger btn-sm" style="width: 80px;">
                    <i class="fa fa-ban"></i> Clear
                  </button>
                </div>
               
              </div>

              
            </form>
          </div>
  </div>
  <div class="control-sidebar-bg"></div>

</div>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<script src="dist/js/adminlte.min.js"></script>
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="bower_components/chart.js/Chart.js"></script>
<script src="dist/js/pages/dashboard2.js"></script>
<script src="dist/js/demo.js"></script>
</body>
</html>
