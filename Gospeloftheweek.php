<?php
session_start();

include "DbConn.php";
$sqlpending = "SELECT intReserveCustomer_ID, strReserveStatus FROM reservationtbl WHERE reservationtbl.strReserveStatus = 'PENDING'";
$countpend = $conn->query($sqlpending);
$totalpending = mysqli_num_rows($countpend);

$sqlinbox = "SELECT * FROM messagetbl WHERE strMessage_ConvoCode = 'usersentunread'";
$countinbox = $conn->query($sqlinbox);  
$totalinbox = mysqli_num_rows($countinbox);
?>
<?php 
if(isset($_SESSION['intUserID']) && !empty($_SESSION['intUserID'])) {
    if($_SESSION['intUserID'] != '1') {
      header ("Location: Signin.php");
    } 
    else {
    }
  }
  else {
    header ("Location: Signin.php");
  }
?>
<?php
      if(isset($_GET['s']) && $_GET['s'] == 'logout') {
      session_destroy();      
      if($conn) {
        $conn->close();
      }
      header("Location: " . $_SERVER['PHP_SELF']);      
      }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Gospel of the Week</title>
  <link rel="icon" type="image/ico" href="OLALOGOmin.png " />
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <a href="index2.php" class="logo">
      <span class="logo-mini"><img src="OLALOGO.png" style="position: relative; left: -18px;" /></span>
      <span class="logo-lg"><img src="OLALOGO.png" style="position: relative; left: -18px;" /></span>
    </a>

    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
    </nav>
  </header>

  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><center>MAIN NAVIGATION</center></li>
        <li class="treeview">
          <a href="index2.php">
            <i class="fa fa-dashboard"></i><span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-list"></i> <span>Transaction</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <small class="label pull-right bg-yellow"><?php echo $totalpending?></small>
              <small class="label pull-right bg-green"></small>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="PendReserve.php"><i class="fa fa-fw fa-circle"></i> Pending Requests</a></li>
            <li><a href="ConfirmReserve.php"><i class="fa fa-fw fa-circle"></i> Confirmed Requests</a></li>
          </ul>
        </li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-fw fa-wrench"></i> <span>Maintenance</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="CalendarOfActivities.php"><i class="fa fa-calendar"></i> Calendar of Activities</a></li>
            <li><a href="Gospeloftheweek.php"><i class="fa fa-fw fa-file-text"></i> Gospel of the Week</a></li>
            <li><a href="Merchandise.php"><i class="fa fa-fw fa-shopping-cart"></i> Church Merchandise</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-users"></i> <span>Profiles</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="PriestProfile.php"><i class="fa fa-fw fa-user"></i>Priest</a></li>
            <li><a href="CustomerProfile.php"><i class="fa fa-fw fa-user"></i>Parishioners</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-file-text"></i> <span>Documents</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="AddDoc.php"><i class="fa fa-fw fa-files-o"></i>Certificates</a></li>
            <li><a href="#"><i class="fa fa-fw fa-files-o"></i>Vouchers</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-envelope"></i> <span>Mailbox</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <small class="label pull-right bg-blue"><?php echo $totalinbox?></small>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="MessageInbox.php"><i class="fa fa-fw fa-circle"></i>Inbox</a></li>
            <li><a href="MessageSent.php"><i class="fa fa-fw fa-circle"></i>Sent Items</a></li>
            <li><a href="MessageCompose.php"><i class="fa fa-fw fa-circle"></i>Compose message</a></li>
          </ul>
        </li>
        <li class="treeview-active">
          <a href="?s=logout">
            <i class="fa fa-sign-out"></i><span>Log out</span>
          </a>
        </li>
    </section>
  </aside>

  <div class="content-wrapper">
   <section class="content-header">
      <h1>
        Gospel of the Week
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-fw fa-wrench"></i>Maintenance</a></li>
        <li class="active">Gospel of the Week</li>
      </ol>
    </section>

  <section class="content">
  <div class="row">
        <?php
         echo "<div class='col-md-8'>";
          echo "<div class='box box-info' style='width:1100px;'>";
            echo "<div class='box-header with-border'>";
               echo "<h3 class=box-title>Gospel List</h3>";
               echo "<div class='box-tools pull-right>";
                  echo "<button class='btn btn-box-tool' data-widget='collapse'><i class='fa fa-minus'></i></button>";
               echo "</div>";
            echo "</div>";
            
            echo "<div class='box-body'>";
              echo "<div class='table-responsive'>";
                echo "<table class='table no-margin'>";
                  echo "<thead>";

                 $select_gospel = "SELECT * FROM `gospeltbl`";
                 $q_select_gospel = $conn->query($select_gospel);
                  if($q_select_gospel -> num_rows > 0) {
                  echo "<tr>";
                    echo "<th style='width: 90px'>Week #</th>";
                    echo "<th style='width: 150px'>Date</th>";
                    echo "<th style='width: 700px'>Gospel</th>";
                    echo "<th>Action</th>";
                  echo "</tr>";
                  echo "</thead>";
                  echo "<tbody>";                    
                      while($gospelrow = $q_select_gospel->fetch_assoc()) {
                        $content = $gospelrow['strGospel_Content'];
                        $string = strlen($content)>80?  substr($content,0,80)."...":$content;
                        $newdate = date("m-d-Y", strtotime($gospelrow['dtmGW_Date']));
                      echo "<tr>";
                      echo "<td style='position: relative; left: 14px;'>",$gospelrow['intGWID'],"</td>";
                       echo "<td>",$newdate,"</td>";
                      echo "<td>",$string,"</td>";
                        echo "<td>";
                        echo "<div style='display:block; float:right; position: relative; right: 100px;'>";
                          echo "<form action='ViewGospeloftheweek.php' method='post'>";
                          echo "<button data-toggle='tooltip' data-placement='top' title='View' name= 'view' value='".$gospelrow['intGWID']."' style='color: #fff; background-color: #337ab7; border-color: #2e6da4; border-radius: 50%;'>";
                          echo "<input type = 'hidden', name = 'strGospel_Content' value = '".$gospelrow['strGospel_Content']."'>";
                            echo "<i class='fa fa-fw fa-eye'>";
                            echo "</i>";
                          echo "</button>";
                          echo "</form>";
                          echo "</div>";
                        echo "<div style='display:block; float:right; position: relative; right: 31px;'>";
                          echo "<form action='EditGospeloftheweek.php' method='post'>";
                          echo "<button data-toggle='tooltip' data-placement='top' title='Edit' name= 'edit' value='".$gospelrow['intGWID']."' style='color: #fff; background-color: #337ab7; border-color: #2e6da4; border-radius: 50%;'>";
                          echo "<input type = 'hidden', name = 'strGospel_Content' value = '".$gospelrow['strGospel_Content']."'>";
                            echo "<i class='fa fa-fw fa-edit'>";
                            echo "</i>";
                          echo "</button>";
                          echo "</form>";
                          echo "</div>";
                    echo "</td>";
                    echo "<tr>";
                  }
                  
            }
            else {
                    echo"<h4><i class='fa fa-info'></i> Note:</h4>";
                    echo "<i>No Content</i>";
                  }
                    
                  echo "</tr>";
                  echo "</tbody>";
                echo "</table>";
              echo "</div>"; 
            echo "</div>";
          echo "</div>";
          ?>

          
      </div>
    </section>

  <div class="control-sidebar-bg"></div>

</div>

<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<script src="dist/js/adminlte.min.js"></script>
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="bower_components/chart.js/Chart.js"></script>
<script src="dist/js/pages/dashboard2.js"></script>
<script src="dist/js/demo.js"></script>
</body>
</html>
