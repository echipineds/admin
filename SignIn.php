<?php
include 'DbConn.php';
session_start();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin Sign in</title>
  <link rel="icon" type="image/ico" href="OLALOGOmin.png " />
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<?php

 function login($username, $password)
 {
  $servername="localhost";
  $username="root";
  $password="";
  $dbname="ola_db";
  $conn= mysqli_connect($servername, $username, $password, $dbname);

  $username = mysqli_real_escape_string($conn, $_POST['strUsername']);
  $password = mysqli_real_escape_string($conn, $_POST['strUserPassword']);
  $query = "SELECT * FROM usertbl WHERE strUsername='$username' AND strUserPassword='$password' AND intUserID = '1' ";

  $result = mysqli_query($conn,$query) or die (mysqli_error());
  $num_row = mysqli_num_rows($result);

  if($num_row == 1)
  {
    while($row=mysqli_fetch_array($result)){
      $_SESSION['intUserID'] = $row['intUserID'];

    }
  }else {
    return false;
  }
    return true;
 }

 if(isset($_POST['submit'])){
  $validLogin = login($_POST['strUsername'], $_POST['strUserPassword']);
  if($validLogin){
    header("Location: Login_Connect.php");  
    exit();
  }else{
    echo"<div class='alert alert-info text-center'>
    <a href='#' class='close' data-dismiss='warning' aria-label='close'>&times;</a>
    Please try Again! Incorrect Username/Password!!!
    </div>";
  }
 }
?>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body" class="animated fadeIn">
    <span class="logo-lg"><img src="OLA logo.png" style="width: 350px; position:relative; left: -15px; " /></span>
    <p class="login-box-msg">Sign in to start your session</p>

    <form action="SignIn.php" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" name="strUsername" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="strUserPassword" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Remember Me
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat" name="submit">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

   
    <!-- /.social-auth-links -->

   

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>
</html>
