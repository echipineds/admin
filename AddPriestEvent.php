<?php
session_start();

  include ('DbConn.php');
$sqlpending = "SELECT intReserveCustomer_ID, strReserveStatus FROM reservationtbl WHERE reservationtbl.strReserveStatus = 'PENDING'";
$countpend = $conn->query($sqlpending);
$totalpending = mysqli_num_rows($countpend);

$sqlinbox = "SELECT * FROM messagetbl WHERE strMessage_ConvoCode = 'usersentunread'";
$countinbox = $conn->query($sqlinbox);  
$totalinbox = mysqli_num_rows($countinbox);
?>
<?php
if(isset($_POST['submit'])){

 $id = $_POST['submit'];
 $dateStart = $_POST['dtDate_Started'];
 $timeStart = $_POST['tmStart_Time'];
 $timeEnd = $_POST['tmEnd_Time'];
 $schedDesc = $_POST['strSched_Desc']; 

 $sql = ("INSERT INTO priestschedtbl(dtDate_Started, tmStart_Time, tmEnd_Time, strSched_Desc, intPriest_ID) VALUES ('$dateStart','$timeStart','$timeEnd','$schedDesc','$id')");



  $result = mysqli_query($conn,$sql);

if($result){
    $AddSched_Msg = "<div class='alert alert-info text-center'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    Schedule has been Added!
    </div>";
    header("refresh:0.5 url=PriestProfile.php");
  }
  else{

    $AddSched_Msg = "<div class='alert alert-info text-center'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    Failed to add Schedule!
    </div>";
    header("refresh: url=PriestProfile.php");
  }

}
  $conn->close(); 

?>


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Add Priest Schedule</title>
  <link rel="icon" type="image/ico" href="OLALOGOmin.png " />
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <a href="index2.php" class="logo">
      <span class="logo-mini"><img src="OLALOGO.png" style="position: relative; left: -18px;" /></span>
      <span class="logo-lg"><img src="OLALOGO.png" style="position: relative; left: -18px;" /></span>
    </a>

    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
    </nav>
  </header>

  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><center>MAIN NAVIGATION</center></li>
        <li class="treeview">
          <a href="index2.php">
            <i class="fa fa-dashboard"></i><span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-list"></i> <span>Transaction</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <small class="label pull-right bg-yellow"><?php echo $totalpending?></small>
              <small class="label pull-right bg-green"></small>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="PendReserve.php"><i class="fa fa-fw fa-circle"></i>Pending Requests</a></li>
            <li><a href="ConfirmReserve.php"><i class="fa fa-fw fa-circle"></i> Confirmed Requests</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-wrench"></i> <span>Maintenance</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="CalendarOfActivities.php"><i class="fa fa-calendar"></i> Calendar of Activities</a></li>
            <li><a href="Gospeloftheweek.php"><i class="fa fa-fw fa-file-text"></i> Gospel of the Week</a></li>
            <li><a href="Merchandise.php"><i class="fa fa-fw fa-shopping-cart"></i> Church Merchandise</a></li>
          </ul>
        </li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-fw fa-users"></i> <span>Profiles</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="PriestProfile.php"><i class="fa fa-fw fa-user"></i>Priest</a></li>
            <li><a href="CustomerProfile.php"><i class="fa fa-fw fa-user"></i>Parishioners</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-file-text"></i> <span>Documents</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="AddDoc.php"><i class="fa fa-fw fa-files-o"></i>Certificates</a></li>
            <li><a href="#"><i class="fa fa-fw fa-files-o"></i>Vouchers</a></li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-envelope"></i> <span>Mailbox</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <small class="label pull-right bg-blue"><?php echo $totalinbox?></small>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="MessageInbox.php"><i class="fa fa-fw fa-circle"></i>Inbox</a></li>
            <li><a href="MessageSent.php"><i class="fa fa-fw fa-circle"></i>Sent Items</a></li>
            <li><a href="MessageCompose.php"><i class="fa fa-fw fa-circle"></i>Compose message</a></li>
          </ul>
        </li>
        <li class="treeview-active">
          <a href="?s=logout">
            <i class="fa fa-sign-out"></i><span>Log out</span>
          </a>
        </li>
    </section>
  </aside>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Add Priest Schedule
      </h1>
      <ol class="breadcrumb">
        <li><i class="fa fa-fw fa-users"></i>Profiles</a></li>
        <li><a href="PriestProfile.php">Priest Profile</a></li>
        <li><a href="ViewPriestSched.php">Priest Schedule</a></li>
        <li class="active">Add Priest Event</li>
      </ol>
      
    </section>

   <section class="content">
      <div class="row">
        <div class="col-md-6">
          <div class="button box box-primary" style="width: 970px;">
            <div class="box-header with-border" >
             
            </div>
           
             <form role="form" method="post" action="AddPriestEvent.php" >
               <?php
              echo @$AddSched_Msg
              ?>
              <div class="box-body">
                <div class="form-group" style="width: 400px;">
                  <label>Date Start</label>
                  <input type="date" class="form-control" id="dtDate_Started" name="dtDate_Started" placeholder="Date Start">
                </div>
                <div class="form-group" style="width: 400px;">
                  <label>Start Time</label>
                  <input type="time" class="form-control" id="tmStart_Time" name="tmStart_Time" placeholder="Start Time">
                </div>
                <div class="form-group" style="width: 400px;">
                  <label>End Time</label>
                  <input type="time" class="form-control" id="tmEnd_Time" name="tmEnd_Time" placeholder="End Time">
                </div>
                <div class="form-group" style="width: 400px;">
                  <label>Description</label>
                  <input type="text" class="form-control" id="strSched_Desc" name="strSched_Desc" placeholder="Description">
                </div>

                 <div class="footer pull-right" style=" -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; ">
                  <button type="submit" name="submit" value="<?php echo $_SESSION['intPriest_ID'];?>" class="btn btn-success btn-sm" style="width: 80px;">
                    <i class="fa fa-plus"></i>&nbspAdd
                  </button>
                  <button type="reset" class="btn btn-danger btn-sm" style="width: 80px;">
                    <i class="fa fa-ban"></i> Clear
                  </button>
                </div>
               
              </div>
              

              
            </form>
          </div>
        </secton>
  </div>
  
  <div class="control-sidebar-bg"></div>

</div>

<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<script src="dist/js/adminlte.min.js"></script>
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="bower_components/chart.js/Chart.js"></script>
<script src="dist/js/pages/dashboard2.js"></script>
<script src="dist/js/demo.js"></script>
</body>
</html>
