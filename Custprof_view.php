<?php
session_start();

include "DbConn.php";
$sqlpending = "SELECT intReserveCustomer_ID, strReserveStatus FROM reservationtbl WHERE reservationtbl.strReserveStatus = 'PENDING'";
$countpend = $conn->query($sqlpending);
$totalpending = mysqli_num_rows($countpend);

$sqlinbox = "SELECT * FROM messagetbl WHERE strMessage_ConvoCode = 'usersentunread'";
$countinbox = $conn->query($sqlinbox);  
$totalinbox = mysqli_num_rows($countinbox);
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Parishioner's profile</title>
  <link rel="icon" type="image/ico" href="OLALOGOmin.png " />
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">

    <a href="index2.php" class="logo">
      <span class="logo-mini"><img src="OLALOGO.png" style="position: relative; left: -18px;" /></span>
      <span class="logo-lg"><img src="OLALOGO.png" style="position: relative; left: -18px;" /></span>
    </a>

    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
    </nav>
  </header>

  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><center>MAIN NAVIGATION</center></li>
        <li class="treeview">
          <a href="index2.php">
            <i class="fa fa-dashboard"></i><span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-list"></i> <span>Transaction</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <small class="label pull-right bg-yellow"><?php echo $totalpending?></small>
              <small class="label pull-right bg-green"></small>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="PendReserve.php"><i class="fa fa-fw fa-check-circle"></i> Pending Requests</a></li>
            <li><a href="ConfirmReserve.php"><i class="fa fa-fw fa-circle"></i> Confirmed Requests</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-wrench"></i> <span>Maintenance</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="CAlendarofActivities.php"><i class="fa fa-calendar"></i> Calendar of Activities</a></li>
 
            <li><a href="Gospeloftheweek.php"><i class="fa fa-fw fa-file-text"></i> Gospel of the Week</a></li>
            <li><a href="Merchandise.php"><i class="fa fa-fw fa-shopping-cart"></i> Church Merchandise</a></li>
          </ul>
        </li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-fw fa-users"></i> <span>Profiles</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="PriestProfile.php"><i class="fa fa-fw fa-user"></i>Priest</a></li>
            <li><a href="CustomerProfile.php"><i class="fa fa-fw fa-user"></i>Parishioners</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-file-text"></i> <span>Documents</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="AddDoc.php"><i class="fa fa-fw fa-files-o"></i>Certificates</a></li>
            <li><a href="#"><i class="fa fa-fw fa-files-o"></i>Vouchers</a></li>
          </ul>
          <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-envelope"></i> <span>Mailbox</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <small class="label pull-right bg-blue"><?php echo $totalinbox?></small>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="MessageInbox.php"><i class="fa fa-fw fa-circle"></i>Inbox</a></li>
            <li><a href="MessageSent.php"><i class="fa fa-fw fa-circle"></i>Sent Items</a></li>
            <li><a href="MessageCompose.php"><i class="fa fa-fw fa-circle"></i>Compose message</a></li>
          </ul>
        </li>
        </li>
        <li class="treeview-active">
          <a href="?s=logout">
            <i class="fa fa-sign-out"></i><span>Log out</span>
          </a>
        </li>
    </section>
  </aside>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Parishioner's Profile
      </h1>
      <ol class="breadcrumb">
        <li><i class="fa fa-fw fa-users"></i>Profiles</li>
        <li><a href="CustomerProfile.php">Parishioners</a></li>
        <li class="active">Parishioner's Profile</li>
      </ol>
      </ol>
    </section>

    <section class="content">
       <?php
       if(isset($_POST['view']))
        {
         $id = $_POST['view'];
         $custid = $_POST['intCustomerID'];
        
        $count_pending = "SELECT intReservationID, strTypeofService, strReserveStatus, dtmDate_Reserved FROM reservationtbl WHERE intReserveCustomer_ID = '$id' AND strReserveStatus = 'PENDING'";
        $count_query_pending = $conn->query($count_pending);
        $totalpend = mysqli_num_rows($count_query_pending);

        $count_confirmed = "SELECT intReservationID, strTypeofService, strReserveStatus, dtmDate_Reserved FROM reservationtbl WHERE intReserveCustomer_ID = '$id' AND strReserveStatus = 'CONFIRMED'";
        $count_query_confirmed = $conn->query($count_confirmed);
        $totalconfirmed = mysqli_num_rows($count_query_confirmed);

        

       $select_userprofile = "SELECT usertbl.intUserID, usertbl.strUsername, usertbl.strUserEmail, customertbl.intCustUserID, customertbl.intCustomerID, customertbl.strCustomer_Name, customertbl.strCustomer_Contact,customertbl.strCustomer_Address FROM usertbl INNER JOIN customertbl ON usertbl.intUserID = customertbl.intCustUserID WHERE intUserID = '$id'";
       $select_query_result = $conn->query($select_userprofile);


      echo "<div class='row'>";
      echo  "<div class='col-md-3'>";
        if($select_query_result -> num_rows > 0) {
          echo "<div class='box box-primary'>";
            echo "<div class='box-body box-profile'>";
             while($usertbl = $select_query_result->fetch_assoc()) {
              echo "<h3 class= profile-username text-center'>"."<center>",$usertbl['strUsername'],"</center>"."</h3>";

              echo "<p class='text-muted text-center'>",$usertbl['strUserEmail'],"</p>";

              echo "<ul class='list-group list-group-unbordered'>";
                echo "<li class='list-group-item'>";
                  echo "<b>Pending</b> <a class='pull-right'>$totalpend</a>";
                echo "</li>";
                echo "<li class='list-group-item'>";
                  echo "<b>Confirmed</b> <a class='pull-right'>$totalconfirmed</a>";
               echo "</li>";
            echo "</div>";
          
        echo "<!-- /.box-body -->";
          echo "</div>";
          echo "</div>";
        

      echo "<div class='col-md-9'>";
          echo "<div class='nav-tabs-custom'>";
            echo "<ul class='nav nav-tabs'>";
              echo "<li class='active'>"."<a href='#details' data-toggle='tab'>Details</a>"."</li>";
            echo "</ul>";
            echo "<div class='tab-content'>";
              echo "<div class='active tab-pane' id='details'>";
              echo "<!-- The details -->"; 
                echo "<div class='box-body'>";
              echo "<dl>";
                echo "<dt>Name</dt>";
                echo "<dd>",$usertbl['strCustomer_Name'],"</dd>";
                echo "<dt>Contact</dt>";
                echo "<dd>",$usertbl['strCustomer_Contact'],"</dd>";
                echo "<dt>Address</dt>";
                echo "<dd>",$usertbl['strCustomer_Address'],"</dd>";
              echo "</dl>";
            echo "</div>";                
              echo "</div>";
              
            
                }
        }
              
            echo "</div>";
          echo "</div>";
        echo "</div>";
      echo "</div>";
    
    
    $select_reservation = "SELECT intReservationID, strTypeofService, strReserveStatus, dtmDate_Reserved FROM reservationtbl WHERE intReserveCustomer_ID = '$custid'";
    $select_query_reserve = $conn->query($select_reservation);
    $totalreserve = mysqli_num_rows($select_query_reserve);

    echo "<div class='row'>";
        echo "<div class='col-xs-12'>";
          echo "<div class='box box-primary'>";
            echo "<div class='box-header'>";
              echo "<h3 class='box-title'>Reservations</h3>";
            if($select_query_reserve -> num_rows > 0) {      
            echo "<div class='box-body table-responsive no-padding'>";
              echo "<table class='table table-hover'>";
                echo "<tr>";
                  echo "<th>Id</th>";
                  echo "<th>Service</th>";
                  echo "<th>Status</th>";
                  echo "<th>Date</th>";
                echo "</tr>";
                echo "<tr>";
                  while($reservetbl = $select_query_reserve->fetch_assoc()) {
                  echo "<td>",$reservetbl['intReservationID'],"</td>";
                  echo "<td>",$reservetbl['strTypeofService'],"</td>";
                  echo "<td>",$reservetbl['strReserveStatus'],"</td>";
                  echo "<td>",$reservetbl['dtmDate_Reserved'],"</td>";
                echo "</tr>";
                       }
  }
              echo "</table>";
            echo "</div>";
          echo "</div>";
        echo "</div>";
      echo "</div>";
    }
    ?>
    </section>
    </div>

<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap  -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS -->
<script src="bower_components/chart.js/Chart.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>