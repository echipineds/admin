<?php
session_start();

  include "DbConn.php";
$sqlpending = "SELECT intReserveCustomer_ID, strReserveStatus FROM reservationtbl WHERE reservationtbl.strReserveStatus = 'PENDING'";
$countpend = $conn->query($sqlpending);
$totalpending = mysqli_num_rows($countpend);

$sqlinbox = "SELECT * FROM messagetbl WHERE strMessage_ConvoCode = 'usersentunread'";
$countinbox = $conn->query($sqlinbox);  
$totalinbox = mysqli_num_rows($countinbox);
?>
<?php
if(isset($_SESSION['intUserID']) && !empty($_SESSION['intUserID'])) {
    if($_SESSION['intUserID'] != '1') {
      header ("Location: Signin.php");
    } 
    else {
    }
  }
  else {
    header ("Location: Signin.php");
  }
  ?>
<?php
      if(isset($_GET['s']) && $_GET['s'] == 'logout') {
      session_destroy();      
      if($conn) {
        $conn->close();
      }
      header("Location: " . $_SERVER['PHP_SELF']);      
      }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Pending Reservations</title>
  <link rel="icon" type="image/ico" href="OLALOGOmin.png " />
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <header class="main-header">
    <a href="index2.php" class="logo">
      <span class="logo-mini"><img src="OLALOGO.png" style="position: relative; left: -18px;" /></span>
      <span class="logo-lg"><img src="OLALOGO.png" style="position: relative; left: -18px;" /></span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
    </nav>
  </header>
 
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><center>MAIN NAVIGATION</center></li>
        <li class="treeview">
          <a href="index2.php">
            <i class="fa fa-dashboard"></i><span>Dashboard</span>
          </a>
        </li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-fw fa-list"></i> <span>Transaction</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <small class="label pull-right bg-yellow"><?php echo $totalpending?></small>
              <small class="label pull-right bg-green"></small>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="PendReserve.php"><i class="fa fa-fw fa-check-circle"></i> Pending Requests</a></li>
            <li><a href="ConfirmReserve.php"><i class="fa fa-fw fa-circle"></i> Confirmed Requests</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-wrench"></i> <span>Maintenance</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="CalendarOfActivities.php"><i class="fa fa-calendar"></i> Calendar of Activities</a></li>
            <li><a href="Gospeloftheweek.php"><i class="fa fa-fw fa-file-text"></i> Gospel of the Week</a></li>
            <li><a href="Merchandise.php"><i class="fa fa-fw fa-shopping-cart"></i> Church Merchandise</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-users"></i> <span>Profiles</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="PriestProfile.php"><i class="fa fa-fw fa-user"></i>Priest</a></li>
            <li><a href="CustomerProfile.php"><i class="fa fa-fw fa-user"></i>Parishioners</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-file-text"></i> <span>Documents</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="AddDoc.php"><i class="fa fa-fw fa-files-o"></i>Certificates</a></li>
            <li><a href="#"><i class="fa fa-fw fa-files-o"></i>Vouchers</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-envelope"></i> <span>Mailbox</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <small class="label pull-right bg-blue"><?php echo $totalinbox?></small>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="MessageInbox.php"><i class="fa fa-fw fa-circle"></i>Inbox</a></li>
            <li><a href="MessageSent.php"><i class="fa fa-fw fa-circle"></i>Sent Items</a></li>
            <li><a href="MessageCompose.php"><i class="fa fa-fw fa-circle"></i>Compose message</a></li>
          </ul>
        </li>
        <li class="treeview-active">
          <a href="?s=logout">
            <i class="fa fa-sign-out"></i><span>Log out</span>
          </a>
        </li>
    </section>
  </aside>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Pending Requests
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-fw fa-list"></i>Transaction</a></li>
        <li class="active">Pending Requests</li>
      </ol>
    </section>

    <section class="content">
      <?php
      echo "<div class='row'>";
        echo "<div class='col-md-4'>";
          echo "<div class='box box-info' style='width:1100px;'>";
            echo "<div class='box-header with-border' style='height: 20px;'>";
            echo "</div>";
            echo "<div class='box-body'>";
              echo "<div class='table-responsive'>";
                echo "<table class='table no-margin'>";
                  echo "<h4>Baptism</h4>";
                  echo "<thead>";
                  
                 $select_all_reserv = "SELECT customertbl.strCustomer_Name, servicetbl.intBaptism_ID, reservationtbl.intReservationID, reservationtbl.dtReserve_Date, reservationtbl.tmReserve_Time, reservationtbl.tmReserve_End_Time, reservationtbl.strReserveStatus, reservationtbl.strTypeofService, reservationtbl.intReserveCustomer_ID FROM customertbl INNER JOIN servicetbl ON customertbl.intCustomerID = servicetbl.intCustomer_ID INNER JOIN reservationtbl ON servicetbl.intCustomer_ID = reservationtbl.intReserveCustomer_ID WHERE reservationtbl.strReserveStatus = 'PENDING' AND reservationtbl.strTypeofService = 'BAPTISM' AND servicetbl.intBaptism_ID IS NOT NULL";
                 
                 $select_query_result = $conn->query($select_all_reserv);

                  if($select_query_result -> num_rows > 0) {        
                  echo "<tr>";
                    echo "<th>Name</th>";
                    echo "<th>Date</th>";
                    echo "<th>Time</th>";
                    echo "<th>Status</th>";
                    echo "<th>Action</th>";
                  echo "</tr>";
                  echo "</thead>";
                  echo "<tbody>";
                  echo "<tr>";                              
                      while($pendrow = $select_query_result->fetch_assoc()) {
                      echo "<td>",$pendrow['strCustomer_Name'],"</td>";
                      echo "<td>",$pendrow['dtReserve_Date'],"</td>";
                      echo "<td>",$pendrow['tmReserve_Time'],"</td>";
                      echo "<td>","<span class='label label-warning'>",$pendrow['strReserveStatus'],"</span>","</td>";
                      echo "<td>";
                      echo "<form role='form' method='post' action='updatepending.php'>";
                        echo "<div>";
                          echo "<button data-toggle='tooltip' data-placement='top' title='Confirm' name= 'confirm' value='".$pendrow['intReserveCustomer_ID']."' style='color: #fff; background-color: #337ab7; border-color: #2e6da4; border-radius: 50%;'>";
                            echo "<input type = 'hidden', name = 'intBaptism_ID' value = '".$pendrow['intBaptism_ID']."'>";
                            echo "<input type = 'hidden', name = 'strTypeofService' value = '".$pendrow['strTypeofService']."'>";
                            echo "<input type = 'hidden', name = 'intReservationID' value = '".$pendrow['intReservationID']."'>";
                            echo "<input type = 'hidden', name = 'strCustomer_Name' value = '".$pendrow['strCustomer_Name']."'>";
                            echo "<input type = 'hidden', name = 'dtReserve_Date' value = '".$pendrow['dtReserve_Date']."'>";
                            echo "<input type = 'hidden', name = 'tmReserve_Time' value = '".$pendrow['tmReserve_Time']."'>";
                            echo "<input type = 'hidden', name = 'tmReserve_End_Time' value = '".$pendrow['tmReserve_End_Time']."'>";
                            echo "<i class='fa fa-fw fa-check-square-o'>";
                            echo "</i>";
                            echo "</button>";
                            echo "<button data-toggle='tooltip' data-placement='top' title='Delete' name= 'delete' value='".$pendrow['intReserveCustomer_ID']."' style='color: #fff; background-color: #337ab7; border-color: #2e6da4; border-radius: 50%;'>";
                            echo "<i class='fa fa-fw fa-trash-o'>";
                            echo "</i>";
                          echo "</button>";
                        echo "</div>";
                        echo "</form>";
                    echo "</td>";
                    echo "<tr>";
                  }
            }
            else {
                  echo"<h4><i class='fa fa-info'></i> Note:</h4>";
                    echo "<i>No Pending Requests</i>";
                  }
                    
                  echo "</tr>";
                  echo "</tbody>";
                echo "</table>";
              echo "</div>";
            echo "</div>";
          echo "</div>";
?>

<?php
      
      echo "<div class='row'>";
        echo "<div class='col-md-4'>";
          echo "<div class='box box-info' style='width:1100px;'>";
            echo "<div class='box-header with-border' style='height: 20px;'>";
            echo "</div>";
            echo "<div class='box-body'>";
              echo "<div class='table-responsive'>";
                echo "<table class='table no-margin'>";
                  echo "<h4>Confirmation</h4>";
                  echo "<thead>";
                  
                 $select_all_reserv2 = "SELECT customertbl.strCustomer_Name, servicetbl.intConf_ID, reservationtbl.dtReserve_Date, reservationtbl.intReservationID, reservationtbl.tmReserve_Time, reservationtbl.tmReserve_End_Time, reservationtbl.strReserveStatus, reservationtbl.strTypeofService, reservationtbl.intReserveCustomer_ID FROM customertbl INNER JOIN servicetbl ON customertbl.intCustomerID = servicetbl.intCustomer_ID INNER JOIN reservationtbl ON servicetbl.intCustomer_ID = reservationtbl.intReserveCustomer_ID WHERE reservationtbl.strReserveStatus = 'PENDING' AND reservationtbl.strTypeofService = 'CONFIRMATION' AND servicetbl.intConf_ID IS NOT NULL";
                 
                 $select_query_result2 = $conn->query($select_all_reserv2);

                  if($select_query_result2 -> num_rows > 0) {        
                  echo "<tr>";
                    echo "<th>Name</th>";
                    echo "<th>Date</th>";
                    echo "<th>Time</th>";
                    echo "<th>Status</th>";
                    echo "<th>Action</th>";
                  echo "</tr>";
                  echo "</thead>";
                  echo "<tbody>";
                  echo "<tr>";                              
                      while($pendrow2 = $select_query_result2->fetch_assoc()) {
                      echo "<td>",$pendrow2['strCustomer_Name'],"</td>";
                      echo "<td>",$pendrow2['dtReserve_Date'],"</td>";
                      echo "<td>",$pendrow2['tmReserve_Time'],"</td>";
                      echo "<td>","<span class='label label-warning'>",$pendrow2['strReserveStatus'],"</span>","</td>";
                      echo "<td>";
                      echo "<form role='form' method='post' action='updatepending.php'>";
                        echo "<div>";
                          echo "<button data-toggle='tooltip' data-placement='top' title='Confirm' name= 'confirm' value='".$pendrow2['intReserveCustomer_ID']."' style='color: #fff; background-color: #337ab7; border-color: #2e6da4; border-radius: 50%;'>";
                            echo "<input type = 'hidden', name = 'intConf_ID' value = '".$pendrow2['intConf_ID']."'>";
                            echo "<input type = 'hidden', name = 'strTypeofService' value = '".$pendrow2['strTypeofService']."'>";
                            echo "<input type = 'hidden', name = 'intReservationID' value = '".$pendrow2['intReservationID']."'>";
                            echo "<input type = 'hidden', name = 'strCustomer_Name' value = '".$pendrow2['strCustomer_Name']."'>";
                            echo "<input type = 'hidden', name = 'dtReserve_Date' value = '".$pendrow2['dtReserve_Date']."'>";
                            echo "<input type = 'hidden', name = 'tmReserve_Time' value = '".$pendrow2['tmReserve_Time']."'>";
                            echo "<input type = 'hidden', name = 'tmReserve_End_Time' value = '".$pendrow2['tmReserve_End_Time']."'>";
                            echo "<i class='fa fa-fw fa-check-square-o'>";
                            echo "</i>";
                          echo "</button>";
                          echo "<button data-toggle='tooltip' data-placement='top' title='Delete' name= 'delete' value='".$pendrow2['intReserveCustomer_ID']."' style='color: #fff; background-color: #337ab7; border-color: #2e6da4; border-radius: 50%;'>";
                            echo "<i class='fa fa-fw fa-trash-o'>";
                            echo "</i>";
                          echo "</button>";
                        echo "</div>";
                        echo "</form>";
                    echo "</td>";
                    echo "<tr>";
                  }
            }
            else {
                  echo"<h4><i class='fa fa-info'></i> Note:</h4>";
                    echo "<i>No Pending Requests</i>";
                  }
                    
                  echo "</tr>";
                  echo "</tbody>";
                echo "</table>";
              echo "</div>";
              
            echo "</div>";
          echo "</div>";
?>

<?php
      
      echo "<div class='row'>";
      
        echo "<div class='col-md-4'>";
         
          echo "<div class='box box-info' style='width:1100px;'>";
            echo "<div class='box-header with-border' style='height: 20px;'>";
             
            echo "</div>";
        
            echo "<div class='box-body'>";
              echo "<div class='table-responsive'>";
                echo "<table class='table no-margin'>";
                  echo "<h4>Matrimony</h4>";
                  echo "<thead>";

                 $select_all_reserv3 = "SELECT customertbl.strCustomer_Name, servicetbl.intWedding_ID, reservationtbl.intReservationID, reservationtbl.dtReserve_Date, reservationtbl.tmReserve_Time, reservationtbl.tmReserve_End_Time, reservationtbl.strReserveStatus, reservationtbl.strTypeofService, reservationtbl.intReserveCustomer_ID FROM customertbl INNER JOIN servicetbl ON customertbl.intCustomerID = servicetbl.intCustomer_ID INNER JOIN reservationtbl ON servicetbl.intCustomer_ID = reservationtbl.intReserveCustomer_ID WHERE reservationtbl.strReserveStatus = 'PENDING' AND reservationtbl.strTypeofService = 'WEDDING' AND servicetbl.intWedding_ID IS NOT NULL";
                 
                 $select_query_result3 = $conn->query($select_all_reserv3);

                  if($select_query_result3 -> num_rows > 0) {        
                  echo "<tr>";
                    echo "<th>Name</th>";
                    echo "<th>Date</th>";
                    echo "<th>Time</th>";
                    echo "<th>Status</th>";
                    echo "<th>Action</th>";
                  echo "</tr>";
                  echo "</thead>";
                  echo "<tbody>";
                  echo "<tr>";                              
                      while($pendrow3 = $select_query_result3->fetch_assoc()) {
                      echo "<td>",$pendrow3['strCustomer_Name'],"</td>";
                      echo "<td>",$pendrow3['dtReserve_Date'],"</td>";
                      echo "<td>",$pendrow3['tmReserve_Time'],"</td>";
                      echo "<td>","<span class='label label-warning'>",$pendrow3['strReserveStatus'],"</span>","</td>";
                      echo "<td>";
                      echo "<form role='form' method='post' action='updatepending.php'>";
                        echo "<div>";
                          echo "<button data-toggle='tooltip' data-placement='top' title='Confirm' name= 'confirm' value='".$pendrow3['intReserveCustomer_ID']."' style='color: #fff; background-color: #337ab7; border-color: #2e6da4; border-radius: 50%;'>";
                            echo "<input type = 'hidden', name = 'intWedding_ID' value = '".$pendrow3['intWedding_ID']."'>";
                            echo "<input type = 'hidden', name = 'strTypeofService' value = '".$pendrow3['strTypeofService']."'>";
                            echo "<input type = 'hidden', name = 'intReservationID' value = '".$pendrow3['intReservationID']."'>";
                            echo "<input type = 'hidden', name = 'strCustomer_Name' value = '".$pendrow3['strCustomer_Name']."'>";
                            echo "<input type = 'hidden', name = 'dtReserve_Date' value = '".$pendrow3['dtReserve_Date']."'>";
                            echo "<input type = 'hidden', name = 'tmReserve_Time' value = '".$pendrow3['tmReserve_Time']."'>";
                            echo "<input type = 'hidden', name = 'tmReserve_End_Time' value = '".$pendrow3['tmReserve_End_Time']."'>";
                            echo "<i class='fa fa-fw fa-check-square-o'>";
                            echo "</i>";
                          echo "</button>";
                          echo "<button data-toggle='tooltip' data-placement='top' title='Delete' name= 'delete' value='".$pendrow3['intReserveCustomer_ID']."' style='color: #fff; background-color: #337ab7; border-color: #2e6da4; border-radius: 50%;'>";
                            echo "<i class='fa fa-fw fa-trash-o'>";
                            echo "</i>";
                          echo "</button>";
                        echo "</div>";
                        echo "</form>";
                    echo "</td>";
                    echo "<tr>";
                  }
            }
            else {
                  echo"<h4><i class='fa fa-info'></i> Note:</h4>";
                    echo "<i>No Pending Requests</i>";
                  }
                    
                  echo "</tr>";
                  echo "</tbody>";
                echo "</table>";
              echo "</div>";
              
            echo "</div>";
          echo "</div>";
?>

<?php
      
      echo "<div class='row'>";
      
        echo "<div class='col-md-4'>";
         
          echo "<div class='box box-info' style='width:1100px;'>";
            echo "<div class='box-header with-border' style='height: 20px;'>";
             
            echo "</div>";
        
            echo "<div class='box-body'>";
              echo "<div class='table-responsive'>";
                echo "<table class='table no-margin'>";
                  echo "<h4>Mass Intention</h4>";
                  echo "<thead>";
                  
                 $select_all_reserv4 = "SELECT customertbl.strCustomer_Name, servicetbl.intMInt_ID, reservationtbl.intReservationID, reservationtbl.strReserveStatus, reservationtbl.strTypeofService, reservationtbl.intReserveCustomer_ID FROM customertbl INNER JOIN servicetbl ON customertbl.intCustomerID = servicetbl.intCustomer_ID INNER JOIN reservationtbl ON servicetbl.intCustomer_ID = reservationtbl.intReserveCustomer_ID WHERE reservationtbl.strReserveStatus = 'PENDING' AND reservationtbl.strTypeofService = 'MASS INTENTION' AND servicetbl.intMInt_ID IS NOT NULL";

                 $select_query_result4 = $conn->query($select_all_reserv4);

                  if($select_query_result4 -> num_rows > 0) {        
                  echo "<tr>";
                    echo "<th>Name</th>";
                    echo "<th>Status</th>";
                    echo "<th>Action</th>";
                  echo "</tr>";
                  echo "</thead>";
                  echo "<tbody>";
                  echo "<tr>";                              
                      while($pendrow4 = $select_query_result4->fetch_assoc()) {
                      echo "<td>",$pendrow4['strCustomer_Name'],"</td>";
                      echo "<td>","<span class='label label-warning'>",$pendrow4['strReserveStatus'],"</span>","</td>";
                      echo "<td>";
                      echo "<form role='form' method='post' action='updatepending.php'>";
                        echo "<div>";
                          echo "<button data-toggle='tooltip' data-placement='top' title='Confirm' name= 'confirm' value='".$pendrow4['intReserveCustomer_ID']."' style='color: #fff; background-color: #337ab7; border-color: #2e6da4; border-radius: 50%;'>";
                            echo "<input type = 'hidden', name = 'intMInt_ID' value = '".$pendrow4['intMInt_ID']."'>";
                            echo "<input type = 'hidden', name = 'strTypeofService' value = '".$pendrow4['strTypeofService']."'>";
                            echo "<input type = 'hidden', name = 'intReservationID' value = '".$pendrow4['intReservationID']."'>";
                            echo "<input type = 'hidden', name = 'strCustomer_Name' value = '".$pendrow4['strCustomer_Name']."'>";
                            echo "<i class='fa fa-fw fa-check-square-o'>";
                            echo "</i>";
                          echo "</button>";
                          echo "<button data-toggle='tooltip' data-placement='top' title='Delete' name= 'delete' value='".$pendrow4['intReserveCustomer_ID']."' style='color: #fff; background-color: #337ab7; border-color: #2e6da4; border-radius: 50%;'>";
                            echo "<i class='fa fa-fw fa-trash-o'>";
                            echo "</i>";
                          echo "</button>";
                        echo "</div>";
                        echo "</form>";
                    echo "</td>";
                    echo "<tr>";
                  }
            }
            else {
                  echo"<h4><i class='fa fa-info'></i> Note:</h4>";
                    echo "<i>No Pending Requests</i>";
                  }
                    
                  echo "</tr>";
                  echo "</tbody>";
                echo "</table>";
              echo "</div>";
              
            echo "</div>";
          echo "</div>";
?>

<?php
      
      echo "<div class='row'>";
      
        echo "<div class='col-md-4'>";
         
          echo "<div class='box box-info' style='width:1100px;'>";
            echo "<div class='box-header with-border' style='height: 20px;'>";
             
            echo "</div>";
        
            echo "<div class='box-body'>";
              echo "<div class='table-responsive'>";
                echo "<table class='table no-margin'>";
                  echo "<h4>Funeral Mass</h4>";
                  echo "<thead>";
                  
                 $select_all_reserv5 = "SELECT customertbl.strCustomer_Name, servicetbl.intFuneralMass_ID, reservationtbl.intReservationID, reservationtbl.strReserveStatus, reservationtbl.dtReserve_Date, reservationtbl.tmReserve_Time, reservationtbl.tmReserve_End_Time, reservationtbl.strTypeofService, reservationtbl.intReserveCustomer_ID FROM customertbl INNER JOIN servicetbl ON customertbl.intCustomerID = servicetbl.intCustomer_ID INNER JOIN reservationtbl ON servicetbl.intCustomer_ID = reservationtbl.intReserveCustomer_ID WHERE reservationtbl.strReserveStatus = 'PENDING' AND reservationtbl.strTypeofService = 'FUNERAL MASS' AND servicetbl.intFuneralMass_ID IS NOT NULL";
                 
                 $select_query_result5 = $conn->query($select_all_reserv5);

                  if($select_query_result5 -> num_rows > 0) {        
                  echo "<tr>";
                    echo "<th>Name</th>";
                    echo "<th>Date</th>";
                    echo "<th>Time</th>";
                    echo "<th>Status</th>";
                    echo "<th>Action</th>";
                  echo "</tr>";
                  echo "</thead>";
                  echo "<tbody>";
                  echo "<tr>";                              
                      while($pendrow5 = $select_query_result5->fetch_assoc()) {
                      echo "<td>",$pendrow5['strCustomer_Name'],"</td>";
                      echo "<td>",$pendrow5['dtReserve_Date'],"</td>";
                      echo "<td>",$pendrow5['tmReserve_Time'],"</td>";
                      echo "<td>","<span class='label label-warning'>",$pendrow5['strReserveStatus'],"</span>","</td>";
                      echo "<td>";
                      echo "<form role='form' method='post' action='updatepending.php'>";
                        echo "<div>";
                          echo "<button data-toggle='tooltip' data-placement='top' title='Confirm' name= 'confirm' value='".$pendrow5['intReserveCustomer_ID']."' style='color: #fff; background-color: #337ab7; border-color: #2e6da4; border-radius: 50%;'>";
                            echo "<input type = 'hidden', name = 'intFuneralMass_ID' value = '".$pendrow5['intFuneralMass_ID']."'>";
                            echo "<input type = 'hidden', name = 'strTypeofService' value = '".$pendrow5['strTypeofService']."'>";
                            echo "<input type = 'hidden', name = 'intReservationID' value = '".$pendrow5['intReservationID']."'>";
                            echo "<input type = 'hidden', name = 'strCustomer_Name' value = '".$pendrow5['strCustomer_Name']."'>";
                            echo "<input type = 'hidden', name = 'dtReserve_Date' value = '".$pendrow5['dtReserve_Date']."'>";
                            echo "<input type = 'hidden', name = 'tmReserve_Time' value = '".$pendrow5['tmReserve_Time']."'>";
                            echo "<input type = 'hidden', name = 'tmReserve_End_Time' value = '".$pendrow5['tmReserve_End_Time']."'>";
                            echo "<i class='fa fa-fw fa-check-square-o'>";
                            echo "</i>";
                          echo "</button>";
                          echo "<button data-toggle='tooltip' data-placement='top' title='Delete' name= 'delete' value='".$pendrow5['intReserveCustomer_ID']."' style='color: #fff; background-color: #337ab7; border-color: #2e6da4; border-radius: 50%;'>";
                            echo "<i class='fa fa-fw fa-trash-o'>";
                            echo "</i>";
                          echo "</button>";
                        echo "</div>";
                        echo "</form>";
                    echo "</td>";
                    echo "<tr>";
                  }
            }
            else {
                  echo"<h4><i class='fa fa-info'></i> Note:</h4>";
                    echo "<i>No Pending Requests</i>";
                  }
                    
                  echo "</tr>";
                  echo "</tbody>";
                echo "</table>";
              echo "</div>";
              
            echo "</div>";
          echo "</div>";
?>
<?php
      
      echo "<div class='row'>";
      
        echo "<div class='col-md-4'>";
         
          echo "<div class='box box-info' style='width:1100px;'>";
            echo "<div class='box-header with-border' style='height: 20px;'>";
             
            echo "</div>";
        
            echo "<div class='box-body'>";
              echo "<div class='table-responsive'>";
                echo "<table class='table no-margin'>";
                  echo "<h4>Blessing</h4>";
                  echo "<thead>";
                  
                 $select_all_reserv6 = "SELECT customertbl.strCustomer_Name, servicetbl.intBlessingServ_ID, reservationtbl.intReservationID, reservationtbl.strReserveStatus, reservationtbl.dtReserve_Date, reservationtbl.tmReserve_Time, reservationtbl.tmReserve_End_Time, reservationtbl.strTypeofService, reservationtbl.intReserveCustomer_ID FROM customertbl INNER JOIN servicetbl ON customertbl.intCustomerID = servicetbl.intCustomer_ID INNER JOIN reservationtbl ON servicetbl.intCustomer_ID = reservationtbl.intReserveCustomer_ID WHERE reservationtbl.strReserveStatus = 'PENDING' AND reservationtbl.strTypeofService = 'BLESSING' AND servicetbl.intBlessingServ_ID IS NOT NULL";
                 
                 $select_query_result6 = $conn->query($select_all_reserv6);

                  if($select_query_result6 -> num_rows > 0) {        
                  echo "<tr>";
                    echo "<th>Name</th>";
                    echo "<th>Date</th>";
                    echo "<th>Time</th>";
                    echo "<th>Status</th>";
                    echo "<th>Action</th>";
                  echo "</tr>";
                  echo "</thead>";
                  echo "<tbody>";
                  echo "<tr>";                              
                      while($pendrow6 = $select_query_result6->fetch_assoc()) {
                      echo "<td>",$pendrow6['strCustomer_Name'],"</td>";
                      echo "<td>",$pendrow6['dtReserve_Date'],"</td>";
                      echo "<td>",$pendrow6['tmReserve_Time'],"</td>";
                      echo "<td>","<span class='label label-warning'>",$pendrow6['strReserveStatus'],"</span>","</td>";
                      echo "<td>";
                        echo "<div>";
                        echo "<form role='form' method='post' action='updatepending.php'>";
                          echo "<button data-toggle='tooltip' data-placement='top' title='Confirm' name= 'confirm' value='".$pendrow6['intReserveCustomer_ID']."' style='color: #fff; background-color: #337ab7; border-color: #2e6da4; border-radius: 50%;'>";
                            echo "<input type = 'hidden', name = 'intBlessingServ_ID' value = '".$pendrow6['intBlessingServ_ID']."'>";
                            echo "<input type = 'hidden', name = 'strTypeofService' value = '".$pendrow6['strTypeofService']."'>";
                            echo "<input type = 'hidden', name = 'intReservationID' value = '".$pendrow6['intReservationID']."'>";
                            echo "<input type = 'hidden', name = 'strCustomer_Name' value = '".$pendrow6['strCustomer_Name']."'>";
                            echo "<input type = 'hidden', name = 'dtReserve_Date' value = '".$pendrow6['dtReserve_Date']."'>";
                            echo "<input type = 'hidden', name = 'tmReserve_Time' value = '".$pendrow6['tmReserve_Time']."'>";
                            echo "<input type = 'hidden', name = 'tmReserve_End_Time' value = '".$pendrow6['tmReserve_End_Time']."'>";
                            echo "<i class='fa fa-fw fa-check-square-o'>";
                            echo "</i>";
                          echo "</button>";
                          echo "<button data-toggle='tooltip' data-placement='top' title='Delete' name= 'delete' value='".$pendrow6['intReserveCustomer_ID']."' style='color: #fff; background-color: #337ab7; border-color: #2e6da4; border-radius: 50%;'>";
                            echo "<i class='fa fa-fw fa-trash-o'>";
                            echo "</i>";
                          echo "</button>";
                        echo "</div>";
                        echo "</form>";
                    echo "</td>";
                    echo "<tr>";
                  }
            }
            else {
                  echo"<h4><i class='fa fa-info'></i> Note:</h4>";
                    echo "<i>No Pending Requests</i>";
                  }
                    
                  echo "</tr>";
                  echo "</tbody>";
                echo "</table>";
              echo "</div>";
              
            echo "</div>";
          echo "</div>";
?>
<?php
      
      echo "<div class='row'>";
      
        echo "<div class='col-md-4'>";
         
          echo "<div class='box box-info' style='width:1100px;'>";
            echo "<div class='box-header with-border' style='height: 20px;'>";
             
            echo "</div>";
        
            echo "<div class='box-body'>";
              echo "<div class='table-responsive'>";
                echo "<table class='table no-margin'>";
                  echo "<h4>Certificates</h4>";
                  echo "<thead>";
                  
                 $select_all_reserv7 = "SELECT customertbl.strCustomer_Name, servicetbl.intCertificateID, reservationtbl.intReservationID, reservationtbl.strReserveStatus, reservationtbl.dtReserve_Date, reservationtbl.tmReserve_Time, reservationtbl.strTypeofService, reservationtbl.intReserveCustomer_ID FROM customertbl INNER JOIN servicetbl ON customertbl.intCustomerID = servicetbl.intCustomer_ID INNER JOIN reservationtbl ON servicetbl.intCustomer_ID = reservationtbl.intReserveCustomer_ID WHERE reservationtbl.strReserveStatus = 'PENDING' AND reservationtbl.strTypeofService = 'CERTIFICATION' AND servicetbl.intCertificateID IS NOT NULL";
                 
                 $select_query_result7 = $conn->query($select_all_reserv7);

                  if($select_query_result7 -> num_rows > 0) {        
                  echo "<tr>";
                    echo "<th>Name</th>";
                    echo "<th>Status</th>";
                    echo "<th>Action</th>";
                  echo "</tr>";
                  echo "</thead>";
                  echo "<tbody>";
                  echo "<tr>";                              
                      while($pendrow7 = $select_query_result7->fetch_assoc()) {
                      echo "<td>",$pendrow7['strCustomer_Name'],"</td>";
                      echo "<td>","<span class='label label-warning'>",$pendrow7['strReserveStatus'],"</span>","</td>";
                      echo "<td>";
                        echo "<div>";
                        echo "<form role='form' method='post' action='updatepending.php'>";
                          echo "<button data-toggle='tooltip' data-placement='top' title='Confirm' name= 'confirm' value='".$pendrow7['intReserveCustomer_ID']."' style='color: #fff; background-color: #337ab7; border-color: #2e6da4; border-radius: 50%;'>";
                            echo "<input type = 'hidden', name = 'intCertificateID' value = '".$pendrow7['intCertificateID']."'>";
                            echo "<input type = 'hidden', name = 'strTypeofService' value = '".$pendrow7['strTypeofService']."'>";
                            echo "<input type = 'hidden', name = 'intReservationID' value = '".$pendrow7['intReservationID']."'>";
                            echo "<input type = 'hidden', name = 'strCustomer_Name' value = '".$pendrow7['strCustomer_Name']."'>";
                            echo "<i class='fa fa-fw fa-check-square-o'>";
                            echo "</i>";
                          echo "</button>";
                          echo "<button data-toggle='tooltip' data-placement='top' title='Delete' name= 'delete' value='".$pendrow7['intReserveCustomer_ID']."' style='color: #fff; background-color: #337ab7; border-color: #2e6da4; border-radius: 50%;'>";
                            echo "<i class='fa fa-fw fa-trash-o'>";
                            echo "</i>";
                          echo "</button>";
                        echo "</div>";
                        echo "</form>";
                    echo "</td>";
                    echo "<tr>";
                  }
            }
            else {
                  echo"<h4><i class='fa fa-info'></i> Note:</h4>";
                    echo "<i>No Pending Requests</i>";
                  }
                    
                  echo "</tr>";
                  echo "</tbody>";
                echo "</table>";
              echo "</div>";
              
            echo "</div>";
          echo "</div>";
?>
    </div>
    </section>

  <div class="control-sidebar-bg"></div>

</div>

<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<script src="dist/js/adminlte.min.js"></script>
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="bower_components/chart.js/Chart.js"></script>
<script src="dist/js/pages/dashboard2.js"></script>
<script src="dist/js/demo.js"></script>
</body>
</html>
