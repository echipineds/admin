<?php
session_start();

include "DbConn.php";
$sqlpending = "SELECT intReserveCustomer_ID, strReserveStatus FROM reservationtbl WHERE reservationtbl.strReserveStatus = 'PENDING'";
$countpend = $conn->query($sqlpending);
$totalpending = mysqli_num_rows($countpend);

$sqlinbox = "SELECT * FROM messagetbl WHERE strMessage_ConvoCode = 'usersentunread'";
$countinbox = $conn->query($sqlinbox);  
$totalinbox = mysqli_num_rows($countinbox);
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Parishioners</title>
  <link rel="icon" type="image/ico" href="OLALOGOmin.png " />
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <a href="index2.php" class="logo">
      <span class="logo-mini"><img src="OLALOGO.png" style="position: relative; left: -18px;" /></span>
      <span class="logo-lg"><img src="OLALOGO.png" style="position: relative; left: -18px;" /></span>
    </a>

    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
    </nav>
  </header>

  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><center>MAIN NAVIGATION</center></li>
        <li class="treeview">
          <a href="index2.php">
            <i class="fa fa-dashboard"></i><span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-list"></i> <span>Transaction</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <small class="label pull-right bg-yellow"><?php echo $totalpending?></small>
              <small class="label pull-right bg-green"></small>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="PendReserve.php"><i class="fa fa-fw fa-circle"></i> Pending Requests</a></li>
            <li><a href="ConfirmReserve.php"><i class="fa fa-fw fa-circle"></i> Confirmed Requests</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-wrench"></i> <span>Maintenance</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="CalendarOfActivities.php"><i class="fa fa-calendar"></i> Calendar of Activities</a></li>
            <li><a href="Gospeloftheweek.php"><i class="fa fa-fw fa-file-text"></i> Gospel of the Week</a></li>
            <li><a href="Merchandise.php"><i class="fa fa-fw fa-shopping-cart"></i> Church Merchandise</a></li>
          </ul>
        </li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-fw fa-users"></i> <span>Profiles</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="PriestProfile.php"><i class="fa fa-fw fa-user"></i>Priest</a></li>
            <li><a href="CustomerProfile.php"><i class="fa fa-fw fa-user"></i>Parishioners</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-file-text"></i> <span>Documents</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="AddDoc.php"><i class="fa fa-fw fa-files-o"></i>Certificates</a></li>
            <li><a href="#"><i class="fa fa-fw fa-files-o"></i>Vouchers</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-envelope"></i> <span>Mailbox</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <small class="label pull-right bg-blue"><?php echo $totalinbox?></small>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="MessageInbox.php"><i class="fa fa-fw fa-circle"></i>Inbox</a></li>
            <li><a href="MessageSent.php"><i class="fa fa-fw fa-circle"></i>Sent Items</a></li>
            <li><a href="MessageCompose.php"><i class="fa fa-fw fa-circle"></i>Compose message</a></li>
          </ul>
        </li>
        <li class="treeview-active">
          <a href="?s=logout">
            <i class="fa fa-sign-out"></i><span>Log out</span>
          </a>
        </li>
    </section>
  </aside>


  <div class="content-wrapper">
    <section class="content-header">
       <h1>
        Parishioners
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-fw fa-users"></i>Profiles</a></li>
        <li class="active">Parishioners</li>
      </ol>
    </section>
 <?php
    echo "<section class='content'>"; 
    echo "<div class='row'>";
        echo "<div class='col-xs-12'>";
          echo "<div class='box box-primary'>";
            echo "<div class='box-header'>";
              echo "<h3 class='box-title'>Customer Profiles</h3>";

              echo "<div class='box-tools'>";
                echo "<div class='input-group input-group-sm' style='width: 150px;'>";
                  echo "<input type='text' name='table_search' class='form-control pull-right' placeholder='Search'>";

                  echo "<div class='input-group-btn'>";
                    echo "<button type='submit' class='btn btn-default'><i class='fa fa-search'></i></button>";
                  echo "</div>";
                echo "</div>";
              echo "</div>";
            echo "</div>";
            $select_all_customer = "SELECT usertbl.intUserID, usertbl.strUsername, usertbl.strUserEmail, customertbl.intCustomerID, customertbl.strCustomer_Name FROM usertbl INNER JOIN customertbl ON usertbl.intUserID = customertbl.intCustUserID";
                 $select_query_result = $conn->query($select_all_customer);

            if($select_query_result -> num_rows > 0) {        
            echo "<div class='box-body table-responsive no-padding'>";
              echo "<table class='table table-hover'>";
                echo "<tr>";
                  echo "<th>Username</th>";
                  echo "<th>Email</th>";
                  echo "<th>Name</th>";
                  echo "<th>Action</th>";
                echo "</tr>";
                echo "<tr>";
                while($custrow = $select_query_result->fetch_assoc()) {
                  echo "<td>",$custrow['strUsername'],"</td>";
                  echo "<td>",$custrow['strUserEmail'],"</td>";
                  echo "<td>",$custrow['strCustomer_Name'],"</td>";
                   echo "<td>";
                  echo "<div style='display:block; float:right; position: relative; right: 180px;'>";
                        echo "<form action='Custprof_view.php' method='post'>";
                          echo "<button type='submit' name='view' data-toggle='tooltip' data-placement='top' title='View Profile' value='".$custrow['intUserID']."' style='color: #fff; background-color: #337ab7; border-color: #2e6da4; border-radius: 50%;'>";
                          echo "<input type = 'hidden', name = 'intCustomerID' value = '".$custrow['intCustomerID']."'>";
                            echo "<i class='fa fa-fw fa-eye'>";
                            echo "</i>";
                          echo "</button>";
                           echo "</form>";
                           echo "</div>";
                            echo "<div style='display: block; float: right; position:relative; right:111px;'>";
                           echo "<form action='Custprof_delete.php' method='post'>";
                          echo "<button type='submit' name='delete' data-toggle='tooltip' data-placement='top' title='Delete' value='".$custrow['intUserID']."' style='color: #fff; background-color: #337ab7; border-color: #2e6da4; border-radius: 50%;'>";
                            echo "<i class='fa fa-fw fa-trash-o'>";
                            echo "</i>";
                          echo "</button>";
                          echo "</form>";
                        echo "</div>";
                  echo "</td>";
                echo "</tr>";
             }
          }
              echo "</table>";
            echo "</div>";
          echo "</div>";
        echo "</div>";
      echo "</div>";
    echo "</section>";
  ?>
    <!-- /.content -->
</div>
<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap  -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS -->
<script src="bower_components/chart.js/Chart.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
